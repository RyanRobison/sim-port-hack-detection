package com.minisoftwareandgames.www.simporthackdetection;

import android.content.Context;
import androidx.room.Room;
import androidx.room.TypeConverters;
import androidx.test.InstrumentationRegistry;
import com.minisoftwareandgames.www.simporthackdetection.RoomDB.*;
import org.joda.time.DateTime;
import org.joda.time.DateTimeComparator;
import org.junit.*;

import java.util.ArrayList;
import java.util.List;

import static com.minisoftwareandgames.www.simporthackdetection.RoomDB.ApplicationServicesChangedEvent.ApplicationServicesChangedEventType.PREFERENCE_OFF;
import static com.minisoftwareandgames.www.simporthackdetection.RoomDB.ApplicationServicesChangedEvent.ApplicationServicesChangedEventType.PREFERENCE_ON;
import static org.junit.Assert.*;

public class DatabaseDaoTests {

	@TypeConverters({Converters.class})
	private ApplicationDatabase applicationDatabase;
	private Context context;
	private static final int HALF_MINUTE_IN_MS = 30000;
	private static final int THREE_HOURS_IN_MS = 10800000;
	private static final int SIX_HOURS_IN_MS = 21600000;
	private static final int TWELVE_HOURS_IN_MS = 43200000;
	private static final int TWENTY_FOUR_HOURS_IN_MS = 86400000;
	private static final int SIGNAL_ENTRY_COUNT = 1500;
	private static final int ON_OFF_ENTRY_COUNT = 100;
	private DateTime now;

	@Before
	public void setUp() {
		context = InstrumentationRegistry.getTargetContext();
		applicationDatabase = Room
				.databaseBuilder(context, ApplicationDatabase.class, "test.db")
				.fallbackToDestructiveMigration()
				.build();
		applicationDatabase.clearAllTables();

		// Fill DB with some data to manipulate
		now = DateTime.now();
		SignalStrengthEntry[] signalStrengthEntries = new SignalStrengthEntry[SIGNAL_ENTRY_COUNT];
		for (int i = 0; i < SIGNAL_ENTRY_COUNT; i++) {
			signalStrengthEntries[i] = new SignalStrengthEntry(
					i % 5, // Level int bound by SignalStrength#getLevel() (API Level 23)
					new DateTime(now.getMillis() + (HALF_MINUTE_IN_MS * i))
			);
		}
		// Intentionally using a bulk insert
		applicationDatabase.signalStrengthsDao().insertLogEntries(signalStrengthEntries);

		boolean preferredOn = false;
		ApplicationServicesChangedEvent[] events = new ApplicationServicesChangedEvent[ON_OFF_ENTRY_COUNT];
		for (int i = 0; i < ON_OFF_ENTRY_COUNT; i++) {
			events[i] = new ApplicationServicesChangedEvent(
					new DateTime(now.getMillis() + (HALF_MINUTE_IN_MS * 5 * i)),
					preferredOn ? PREFERENCE_ON : PREFERENCE_OFF);
			preferredOn = !preferredOn;
		}
		applicationDatabase.eventsDao().insertApplicationServicesChangedEvent(events);
	}

	@After
	public void tearDown() {
		applicationDatabase.clearAllTables();
		assertEquals(
				"@Query for all entries should return an empty list now that the tables have been cleared",
				0,
				applicationDatabase.signalStrengthsDao().getAllSignalLogs().size());
		applicationDatabase.close();
	}

	/**
	 * In addition to the existing data, we should be able to insert a new element and assert that it was successfully
	 * added to the database.
	 */
	@Test
	public void testInsert() {
		DateTime now = DateTime.now();
		long[] uids = applicationDatabase
				.signalStrengthsDao()
				.insertLogEntries(new SignalStrengthEntry(5, now));
		assertEquals("@Insert should return a single `long` uid.", 1, uids.length);
		List<SignalStrengthEntry> signalStrengthEntries = applicationDatabase
				.signalStrengthsDao()
				.findSignalLogsBetweenDates(now, now);
		assertNotNull("@Query for exact date should not return a null list.", signalStrengthEntries);
		assertEquals(
				"@Query for exact date should return a single entry.",
				1,
				signalStrengthEntries.size());
		assertEquals(5, signalStrengthEntries.get(0).signalStrengthLevel);
		assertEquals(now, signalStrengthEntries.get(0).dateTime);
	}

	/**
	 * Given the @Before data entry, there is 1500 * 30s worth of data (12.5 "hours"). We should be able to calculate
	 * how many entries we can assert are returned with varying Joda.DateTime intervals, given that all data points are
	 * 30 seconds apart.
	 *
	 * We remove 1 from the millis on each time range so that the ranges are exclusive of the entry that falls on the
	 * exact datetime that matches.
	 */
	@Test
	public void testQueryForDateRange() {
		DateTime threeHoursFromNow = new DateTime(now.getMillis() + THREE_HOURS_IN_MS - 1);
		DateTime sixHoursFromNow = new DateTime(now.getMillis() + SIX_HOURS_IN_MS - 1);
		DateTime twelveHoursFromNow = new DateTime(now.getMillis() + TWELVE_HOURS_IN_MS - 1);
		DateTime twentyFourHoursFromNow = new DateTime(now.getMillis() + TWENTY_FOUR_HOURS_IN_MS - 1);

		assertEquals("@Query for 3 hour date range", THREE_HOURS_IN_MS / HALF_MINUTE_IN_MS,
				applicationDatabase.signalStrengthsDao().findSignalLogsBetweenDates(now, threeHoursFromNow).size());
		assertEquals("@Query for 6 hour date range", SIX_HOURS_IN_MS / HALF_MINUTE_IN_MS,
				applicationDatabase.signalStrengthsDao().findSignalLogsBetweenDates(now, sixHoursFromNow).size());
		assertEquals("@Query for 12 hour date range", TWELVE_HOURS_IN_MS / HALF_MINUTE_IN_MS,
				applicationDatabase.signalStrengthsDao().findSignalLogsBetweenDates(now, twelveHoursFromNow).size());
		assertEquals("@Query for 24 hour date range (all data)", SIGNAL_ENTRY_COUNT,
				applicationDatabase.signalStrengthsDao().findSignalLogsBetweenDates(now, twentyFourHoursFromNow).size());
	}

	@Test
	public void testQueryForAllEntries() {
		assertEquals(String.format("@Query for all data should return %s entries", SIGNAL_ENTRY_COUNT),
				SIGNAL_ENTRY_COUNT, applicationDatabase.signalStrengthsDao().getAllSignalLogs().size());
	}

	@Test
	public void testCombinationQueryIsSorted() {
		List<ApplicationServicesChangedEvent> events = applicationDatabase.eventsDao()
				.findApplicationServicesChangedEventsBetweenDates(
						DateTime.now().minusDays(100),
						DateTime.now().plusDays(100)
				);
		assertEquals(ON_OFF_ENTRY_COUNT, events.size());
		List<SignalStrengthEntry> signalStrengthEntries = applicationDatabase.signalStrengthsDao()
				.findSignalLogsBetweenDates(
						DateTime.now().minusDays(100),
						DateTime.now().plusDays(100)
				);
		assertEquals(SIGNAL_ENTRY_COUNT, signalStrengthEntries.size());
		List<JoinedType> joinedTypes = JoinedType.joinAndSortTypes(signalStrengthEntries, events);
		List<JoinedType> sortedJoinedTypes = new ArrayList<>(joinedTypes);
		sortedJoinedTypes.sort(JoinedType::compareDateTimes);
		for (int i = 0; i < SIGNAL_ENTRY_COUNT + ON_OFF_ENTRY_COUNT; i++) {
			assertEquals(
					String.format("Index: %s\nExpected: %s\nActual:   %s", i, joinedTypes.get(i).toString(), sortedJoinedTypes.get(i).toString()),
					joinedTypes.get(i),
					sortedJoinedTypes.get(i));
		}
	}

}
