package com.minisoftwareandgames.www.simporthackdetection;

import android.content.Context;
import androidx.test.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class MainActivityInstrumentedTests {

	static String packageName = "com.minisoftwareandgames.www.simporthackdetection";

	@Test
	public void mainActivityTests() {
		Context appContext = InstrumentationRegistry.getTargetContext();
		assertEquals(packageName, appContext.getPackageName());
	}

}
