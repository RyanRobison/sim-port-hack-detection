package com.minisoftwareandgames.www.simporthackdetection;

import android.util.Log;
import com.minisoftwareandgames.www.simporthackdetection.RoomDB.ApplicationServicesChangedEvent;
import com.minisoftwareandgames.www.simporthackdetection.RoomDB.JoinedType;
import com.minisoftwareandgames.www.simporthackdetection.RoomDB.SignalStrengthEntry;
import org.joda.time.DateTimeComparator;

import java.util.ArrayList;
import java.util.List;

public class ClockViewHelper {
	
	private float start;
	private float end;
	private float targetStart;
	private float targetEnd;
	private int signalStrength;
	int velocity = 10;

	ClockViewHelper(float startDegree, float endDegree, ClockViewHelper targetPie) {
		start = startDegree;
		end = endDegree;
		this.signalStrength = targetPie.signalStrength;
		targetStart = targetPie.getStart();
		targetEnd = targetPie.getEnd();
	}

	public ClockViewHelper(int startHour, int startMin, int endHour, int endMin, int signalStrength) {
		start = 270 + startHour * 15 + startMin * 15 / 60;
		end = 270 + endHour * 15 + endMin * 15 / 60;
		this.signalStrength = signalStrength;
		while (end < start) {
			end += 360;
		}
	}

	public ClockViewHelper(int startHour, int startMin, int startSec, int endHour, int endMin, int endSec, int signalStrength) {
		start = 270 + startHour * 15 + startMin * 15 / 60 + startSec * 15 / 3600;
		end = 270 + endHour * 15 + endMin * 15 / 60 + endSec * 15 / 3600;
		this.signalStrength = signalStrength;
		while (end < start) {
			end += 360;
		}
	}

	ClockViewHelper setTarget(float targetStart, float targetEnd) {
		this.targetStart = targetStart;
		this.targetEnd = targetEnd;
		return this;
	}

	ClockViewHelper setTarget(ClockViewHelper targetPie) {
		targetStart = targetPie.getStart();
		targetEnd = targetPie.getEnd();
		return this;
	}

	boolean isAtRest() {
		return (start == targetStart) && (end == targetEnd);
	}

	void update() {
		start = updateSelf(start, targetStart, velocity);
		end = updateSelf(end, targetEnd, velocity);
	}

	public float getSweep() {
		return end - start;
	}

	public float getStart() {
		return start;
	}

	public float getEnd() {
		return end;
	}

	public int getSignalStrength() {
		return signalStrength;
	}

	private float updateSelf(float origin, float target, int velocity) {
		if (origin < target) {
			origin += velocity;
		} else if (origin > target) {
			origin -= velocity;
		}
		if (Math.abs(target - origin) < velocity) {
			origin = target;
		}
		return origin;
	}

	public static List<ClockViewHelper> getHelperData(List<SignalStrengthEntry> signalStrengthEntries, List<ApplicationServicesChangedEvent> applicationServicesChangedEvents) {
		List<JoinedType> mergedLists = JoinedType.joinAndSortTypes(signalStrengthEntries, applicationServicesChangedEvents);

		ArrayList<ClockViewHelper> clockViewHelpers = new ArrayList<>();
		SignalStrengthEntry temp = null;
		for (int i = 0; i < mergedLists.size(); i++) {
			JoinedType current = mergedLists.get(i);
			if (temp == null && current.signalStrengthEntry != null) {
				temp = current.signalStrengthEntry;
			} else if (temp != null) {
				if (current.signalStrengthEntry != null) {
					SignalStrengthEntry entry = current.signalStrengthEntry;
					if (temp.signalStrengthLevel != entry.signalStrengthLevel || i == (mergedLists.size() - 1)) {
						clockViewHelpers.add(new ClockViewHelper(
								temp.dateTime.getHourOfDay(),
								temp.dateTime.getMinuteOfHour(),
								temp.dateTime.getSecondOfMinute(),
								entry.dateTime.getHourOfDay(),
								entry.dateTime.getMinuteOfHour(),
								entry.dateTime.getSecondOfMinute(),
								temp.signalStrengthLevel));
						temp = entry;
					}
				} else {
					ApplicationServicesChangedEvent applicationServicesChangedEvent = current.applicationServicesChangedEvent;
					if (applicationServicesChangedEvent.applicationServicesChangedEventType.isOffEventType() || i == (mergedLists.size() - 1)) {
						clockViewHelpers.add(new ClockViewHelper(
								temp.dateTime.getHourOfDay(),
								temp.dateTime.getMinuteOfHour(),
								temp.dateTime.getSecondOfMinute(),
								applicationServicesChangedEvent.datetime.getHourOfDay(),
								applicationServicesChangedEvent.datetime.getMinuteOfHour(),
								applicationServicesChangedEvent.datetime.getSecondOfMinute(),
								temp.signalStrengthLevel));
						temp = null;
					}
				}
			}
		}
		Log.d(ClockViewHelper.class.getSimpleName(), "clockViewHelper List Count: " + clockViewHelpers.size());
		return clockViewHelpers;
	}

	@Override
	public String toString() {
		return String.format("{start: %s, end: %s, sweep: %s, signalStrength: %s}", start, end, getSweep(), signalStrength);
	}

}
