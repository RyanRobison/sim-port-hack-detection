package com.minisoftwareandgames.www.simporthackdetection;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.minisoftwareandgames.www.simporthackdetection.RoomDB.SignalStrengthEntry;

import java.util.ArrayList;
import java.util.List;

import static com.minisoftwareandgames.www.simporthackdetection.RoomDB.Converters.dateToUiString;

public class SignalStrengthLogEntryAdapter extends RecyclerView.Adapter<SignalStrengthLogEntryAdapter.MyViewHolder> {

	private List<SignalStrengthEntry> signalStrengthEntryDataset;
	private ViewColors viewColors;

	public SignalStrengthLogEntryAdapter(ViewColors viewColors) {
		this.viewColors = viewColors;
	}

	static class MyViewHolder extends RecyclerView.ViewHolder {
		LinearLayout horizontalLinearLayout;
		TextView signalStrengthLevelTextView;
		TextView dateTimeTextView;

		MyViewHolder(LinearLayout linearLayout) {
			super(linearLayout);
			horizontalLinearLayout = linearLayout;
			signalStrengthLevelTextView = linearLayout.findViewById(R.id.signalStrengthLevelTextView);
			dateTimeTextView = linearLayout.findViewById(R.id.datetimeTextView);
		}
	}

	@Override
	public SignalStrengthLogEntryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		// create a new view
		LinearLayout logEntryView = (LinearLayout) LayoutInflater.from(parent.getContext())
				.inflate(R.layout.log_entry, parent, false);

		MyViewHolder logEntryViewHolder = new MyViewHolder(logEntryView);
		return logEntryViewHolder;
	}

	@Override
	public void onBindViewHolder(MyViewHolder holder, int position) {
		SignalStrengthEntry entry = signalStrengthEntryDataset.get(position);
		// - get element from your dataset at this position
		// - replace the contents of the view with that element
		holder.signalStrengthLevelTextView.setText(String.valueOf(entry.signalStrengthLevel));

		holder.horizontalLinearLayout.setBackgroundColor(viewColors.getColor(entry.signalStrengthLevel));
		holder.dateTimeTextView.setText(dateToUiString(entry.dateTime));
	}

	@Override
	public int getItemCount() {
		return signalStrengthEntryDataset == null ? 0 : signalStrengthEntryDataset.size();
	}

	public void setSignalStrengthEntryDataset(List<SignalStrengthEntry> dataset) {
		signalStrengthEntryDataset = dataset;
		notifyDataSetChanged();
	}

	public boolean isEmpty() {
		if (signalStrengthEntryDataset == null) {
			signalStrengthEntryDataset = new ArrayList<>();
		}
		return signalStrengthEntryDataset.isEmpty();
	}
}
