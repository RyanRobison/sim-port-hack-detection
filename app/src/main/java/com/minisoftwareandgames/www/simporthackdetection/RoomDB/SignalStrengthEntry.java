package com.minisoftwareandgames.www.simporthackdetection.RoomDB;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import org.joda.time.DateTime;

@Entity(tableName = "signal_logs")
public class SignalStrengthEntry {
	@PrimaryKey(autoGenerate = true)
	public long uid;

	@ColumnInfo(name = "signalStrengthLevel")
	public int signalStrengthLevel;

	@ColumnInfo(name = "dateTime")
	public DateTime dateTime;

	public SignalStrengthEntry(int signalStrengthLevel, DateTime dateTime) {
		this.signalStrengthLevel = signalStrengthLevel;
		this.dateTime = dateTime;
	}

	public static int compareSignalStrengths(SignalStrengthEntry entry1, SignalStrengthEntry entry2) {
		return Integer.compare(entry1.signalStrengthLevel, entry2.signalStrengthLevel);
	}

	public boolean isZeroLevelStrength() {
		return signalStrengthLevel == 0;
	}

	@Override
	public String toString() {
		return String.format("{uid: %s, signalStrengthLevel: %s, dateTime: %s}", uid, signalStrengthLevel, dateTime);
	}
}
