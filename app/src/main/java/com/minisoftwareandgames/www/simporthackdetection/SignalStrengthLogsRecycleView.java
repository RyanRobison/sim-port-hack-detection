package com.minisoftwareandgames.www.simporthackdetection;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.minisoftwareandgames.www.simporthackdetection.RoomDB.SignalStrengthEntry;

import java.util.List;

public class SignalStrengthLogsRecycleView extends RecyclerView implements View.OnScrollChangeListener, View.OnClickListener {

	public SignalStrengthLogsRecycleView(@NonNull Context context) {
		super(context);
		this.setOnScrollChangeListener(this::onScrollChange);
	}

	public SignalStrengthLogsRecycleView(@NonNull Context context, @Nullable AttributeSet attrs) {
		super(context, attrs);
		this.setOnScrollChangeListener(this::onScrollChange);
	}

	public SignalStrengthLogsRecycleView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.setOnScrollChangeListener(this::onScrollChange);
	}

	private FloatingActionButton floatingActionButton;

	public void setFloatingActionButton(FloatingActionButton floatingActionButton) {
		this.floatingActionButton = floatingActionButton;
		this.floatingActionButton.setOnClickListener(this::onClick);
	}

	public boolean isAtBottom() {
		final int DOWN = 1;
		return !this.canScrollVertically(DOWN);
	}

	public void handleNewEntries(List<SignalStrengthEntry> entries) {
		SignalStrengthLogEntryAdapter adapter = (SignalStrengthLogEntryAdapter) getAdapter();
		if (adapter == null) return;
		if (isAtBottom() || adapter.isEmpty()) {
			adapter.setSignalStrengthEntryDataset(entries);
			scrollToPosition(getAdapter().getItemCount() - 1);
		} else {
			// TODO: show user there are new items
			adapter.setSignalStrengthEntryDataset(entries);
		}
	}

	/**
	 * Called when the scroll position of a view changes.
	 *
	 * @param v          The view whose scroll position has changed.
	 * @param scrollX    Current horizontal scroll origin.
	 * @param scrollY    Current vertical scroll origin.
	 * @param oldScrollX Previous horizontal scroll origin.
	 * @param oldScrollY Previous vertical scroll origin.
	 */
	@Override
	public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
		if (isAtBottom()) {
			floatingActionButton.setVisibility(INVISIBLE);
		} else {
			floatingActionButton.setVisibility(VISIBLE);
		}
	}

	/**
	 * Intended specifically for the floatingActionButton
	 *
	 * @param v The view that was clicked.
	 */
	@Override
	public void onClick(View v) {
		if (getAdapter() != null) {
			Log.i(this.getClass().getName(), "FAB clicked - scrolling");
			this.scrollToPosition(getAdapter().getItemCount() - 1);
		}
	}
}
