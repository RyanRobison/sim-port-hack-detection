package com.minisoftwareandgames.www.simporthackdetection.RoomDB;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import org.joda.time.DateTime;

import java.util.List;

@Dao
public interface SignalStrengthsDao {
	@Query("SELECT * FROM signal_logs WHERE datetime BETWEEN :from AND :to")
	LiveData<List<SignalStrengthEntry>> findSignalLogsBetweenDatesLiveData(DateTime from, DateTime to);

	@Query("SELECT * FROM signal_logs WHERE datetime BETWEEN :from AND :to")
	List<SignalStrengthEntry> findSignalLogsBetweenDates(DateTime from, DateTime to);

	@Query("SELECT * FROM signal_logs")
	LiveData<List<SignalStrengthEntry>> allSignalLogsLiveData();

	@Query("SELECT * FROM signal_logs")
	List<SignalStrengthEntry> getAllSignalLogs();

	@Query("SELECT * FROM signal_logs WHERE signalStrengthLevel IS 0 ORDER BY datetime ASC")
	List<SignalStrengthEntry> getAllOutageLogs();

	@Query("SELECT * FROM signal_logs ORDER BY datetime ASC LIMIT 1")
	SignalStrengthEntry getFirstSignalLog();

	@Insert
	long[] insertLogEntries(SignalStrengthEntry... signalStrengthEntry);
}
