package com.minisoftwareandgames.www.simporthackdetection.RoomDB;

import androidx.annotation.NonNull;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

public class Migrations {

	public static Migration[] getAllMigrations() {
		return new Migration[] {
				MIGRATION_1_2
		};
	}

	static final Migration MIGRATION_1_2 = new Migration(1, 2) {
		@Override
		public void migrate(SupportSQLiteDatabase database) {
			database.execSQL("CREATE TABLE application_services_changed_events (uid INTEGER NOT NULL, datetime INTEGER, applicationServicesChangedEventType TEXT, PRIMARY KEY(uid))");
		}
	};

}
