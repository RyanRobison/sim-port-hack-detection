package com.minisoftwareandgames.www.simporthackdetection.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.*;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;
import com.minisoftwareandgames.www.simporthackdetection.R;
import com.minisoftwareandgames.www.simporthackdetection.RoomDB.ApplicationServicesChangedEvent;
import com.minisoftwareandgames.www.simporthackdetection.RoomDB.ApplicationDatabase;
import com.minisoftwareandgames.www.simporthackdetection.multi_preferences.MultiPreferences;
import com.minisoftwareandgames.www.simporthackdetection.services.SignalStrengthAlarmSchedulerService;
import com.minisoftwareandgames.www.simporthackdetection.services.SignalStrengthForegroundNotificationService;
import org.joda.time.DateTime;

import static androidx.appcompat.app.AppCompatDelegate.MODE_NIGHT_NO;
import static androidx.appcompat.app.AppCompatDelegate.MODE_NIGHT_YES;
import static androidx.fragment.app.FragmentManager.POP_BACK_STACK_INCLUSIVE;
import static com.minisoftwareandgames.www.simporthackdetection.RoomDB.ApplicationServicesChangedEvent.ApplicationServicesChangedEventType.PREFERENCE_OFF;
import static com.minisoftwareandgames.www.simporthackdetection.RoomDB.ApplicationServicesChangedEvent.ApplicationServicesChangedEventType.PREFERENCE_ON;
import static com.minisoftwareandgames.www.simporthackdetection.ViewColors.getColorFromAttribute;
import static com.minisoftwareandgames.www.simporthackdetection.activites.MainActivity.setStatusBarStyleAccordingToDarkThemeValue;

public class SettingsFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {

	public static final String MONITOR = "monitor";
	public static final String DARK_THEME = "dark_theme";
	public static final String ACTIVE_NOTIFICATIONS = "active_notifications";
	public static final String PERSISTENT_NOTIFICATIONS = "persistent_notifications";
	private static final String PREFERENCES_NAME = "preferences";
	private View view;
	private Context context;
	private ApplicationDatabase applicationDatabase;
	public static MultiPreferences preferences;

	public static MultiPreferences getPreferences(Context context) {
		if (preferences == null) {
			preferences = new MultiPreferences(PREFERENCES_NAME, context.getApplicationContext());
			SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
			preferences.setBoolean(MONITOR, sharedPreferences.getBoolean(MONITOR, true));
			preferences.setBoolean(DARK_THEME, sharedPreferences.getBoolean(DARK_THEME, false));
			preferences.setBoolean(ACTIVE_NOTIFICATIONS, sharedPreferences.getBoolean(ACTIVE_NOTIFICATIONS, true));
			preferences.setBoolean(PERSISTENT_NOTIFICATIONS, sharedPreferences.getBoolean(PERSISTENT_NOTIFICATIONS, true));
		}
		return preferences;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = getContext();
		applicationDatabase = ApplicationDatabase.getInstance(context);
		getPreferences(context);
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		view = super.onCreateView(inflater, container, savedInstanceState);
		view.setBackgroundColor(getColorFromAttribute(R.attr.backgroundColor, context.getTheme()));
		setupActionBar();
		return view;
	}

	@Override
	public void onConfigurationChanged(@NonNull Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// This changes the background immediately, before the Settings Fragment is replaced providing the look of the status bar and toolbar are synced with the background
		view.setBackgroundColor(getColorFromAttribute(R.attr.backgroundColor, context.getTheme()));
	}

	@Override
	public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
		setPreferencesFromResource(R.xml.preferences_screen, rootKey);
	}

	@Override
	public void onPrepareOptionsMenu(@NonNull Menu menu) {
		MenuItem cog = menu.findItem(R.id.settings_item);
		if (cog != null) cog.setVisible(false);

		MenuItem today = menu.findItem(R.id.go_to_today_item);
		if (today != null) today.setVisible(false);
	}

	private void setupActionBar() {
		ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
		actionBar.setTitle(R.string.settings);
		actionBar.setBackgroundDrawable(new ColorDrawable(getColorFromAttribute(R.attr.backgroundColor, getContext().getTheme())));
	}

	@Override
	public void onResume() {
		super.onResume();
		getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		MultiPreferences preferences = getPreferences(context.getApplicationContext());
		if (MONITOR.equals(key)) {
			boolean isMonitoringEnabled = sharedPreferences.getBoolean(MONITOR, true);
			if (isMonitoringEnabled) {
				SignalStrengthForegroundNotificationService.startService(context);
				SignalStrengthAlarmSchedulerService.startService(context);
				applicationDatabase.eventsDao()
						.insertApplicationServicesChangedEvent(new ApplicationServicesChangedEvent(DateTime.now(), PREFERENCE_ON));
			} else {
				SignalStrengthForegroundNotificationService.stopService(context);
				SignalStrengthAlarmSchedulerService.stopService(context);
				applicationDatabase.eventsDao()
						.insertApplicationServicesChangedEvent(new ApplicationServicesChangedEvent(DateTime.now(), PREFERENCE_OFF));
			}
			preferences.setBoolean(MONITOR, isMonitoringEnabled);
		} else if (DARK_THEME.equals(key)) {
			boolean isDarkTheme = sharedPreferences.getBoolean(DARK_THEME, false);
			context.setTheme(isDarkTheme ? R.style.DarkTheme : R.style.LightTheme);
			AppCompatDelegate.setDefaultNightMode(isDarkTheme ? MODE_NIGHT_YES : MODE_NIGHT_NO);
			setStatusBarStyleAccordingToDarkThemeValue(isDarkTheme, context);
			setupActionBar();
			getFragmentManager()
					.popBackStack(SettingsFragment.class.getSimpleName(), POP_BACK_STACK_INCLUSIVE);
			getFragmentManager()
					.beginTransaction()
					.replace(R.id.fragment, new SettingsFragment(), SettingsFragment.class.getSimpleName())
					.addToBackStack(SettingsFragment.class.getSimpleName())
					.commit();
			preferences.setBoolean(DARK_THEME, isDarkTheme);
		} else if (ACTIVE_NOTIFICATIONS.equals(key)) {
			boolean isActive = sharedPreferences.getBoolean(ACTIVE_NOTIFICATIONS, true);
			preferences.setBoolean(ACTIVE_NOTIFICATIONS, isActive);
		} else if (PERSISTENT_NOTIFICATIONS.equals(key)) {
			boolean isPersistent = sharedPreferences.getBoolean(PERSISTENT_NOTIFICATIONS, true);
			preferences.setBoolean(PERSISTENT_NOTIFICATIONS, isPersistent);
		}
	}
}
