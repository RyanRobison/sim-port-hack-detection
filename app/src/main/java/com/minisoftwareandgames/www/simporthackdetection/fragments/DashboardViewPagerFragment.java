package com.minisoftwareandgames.www.simporthackdetection.fragments;

import android.os.Bundle;
import android.view.*;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import com.minisoftwareandgames.www.simporthackdetection.R;

public class DashboardViewPagerFragment extends Fragment {

	public static String BUNDLE_VIEW_PAGES = "ViewPagesCount";
	private int viewPagesCount = 7;
	private static int OFFSET_FROM_ZERO = 1;
	private ViewPager viewPager;

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
			viewPagesCount = getArguments().getInt(BUNDLE_VIEW_PAGES, viewPagesCount) + OFFSET_FROM_ZERO;
		}
		setHasOptionsMenu(true);
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		setupActionBar();
		View view = inflater.inflate(R.layout.fragment_daily_dashboard_viewpager, container, false);
		viewPager = view.findViewById(R.id.daily_dashboard_viewpager);
		viewPager.setAdapter(new ScreenSlidePagerAdapter(getChildFragmentManager(), viewPagesCount));
		viewPager.setCurrentItem(((ScreenSlidePagerAdapter) viewPager.getAdapter()).reversePosition(OFFSET_FROM_ZERO));
		return view;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.go_to_today_item) {
			viewPager.setCurrentItem(viewPagesCount - 2, true);
		}
		return super.onOptionsItemSelected(item);
	}

	private void setupActionBar() {
		ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
		actionBar.setTitle("Daily Dashboard");
	}

	private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

		private int count = 0;

		public ScreenSlidePagerAdapter(FragmentManager fm, int count) {
			super(fm);
			this.count = count;
		}

		@Override
		public Fragment getItem(int position) {
			if (position == viewPagesCount - 1) {
				SummaryDashboardFragment summaryDashboardFragment = new SummaryDashboardFragment();
				summaryDashboardFragment.setParentViewPager(viewPager);
				return summaryDashboardFragment;
			} else {
				Fragment fragment = new DailyDashboardFragment();
				Bundle bundle = new Bundle();
				bundle.putInt("page", reversePosition(position) - OFFSET_FROM_ZERO);
				fragment.setArguments(bundle);
				return fragment;
			}
		}

		@Override
		public int getCount() {
			return count;
		}

		public int reversePosition(int position) {
			return getCount() - 1 - position;
		}
	}

}
