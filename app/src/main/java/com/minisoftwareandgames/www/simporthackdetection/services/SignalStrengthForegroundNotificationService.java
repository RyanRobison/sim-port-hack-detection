package com.minisoftwareandgames.www.simporthackdetection.services;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import androidx.annotation.Nullable;
import com.minisoftwareandgames.www.simporthackdetection.SignalStrengthListener;
import com.minisoftwareandgames.www.simporthackdetection.notifications.RecurringNotification;
import com.minisoftwareandgames.www.simporthackdetection.notifications.SimStatusNotification;

public class SignalStrengthForegroundNotificationService extends Service {

	private TelephonyManager telephonyManager;
	private SignalStrengthListener signalStrengthListener;

	private boolean isOnSchedule;

	public boolean isOnSchedule() {
		return isOnSchedule;
	}

	public void setOnSchedule(boolean onSchedule) {
		isOnSchedule = onSchedule;
	}

	/**
	 * Called by the system when the service is first created.  Do not call this method directly.
	 */
	@Override
	public void onCreate() {
		super.onCreate();
		SimStatusNotification.startForegroundServiceNotification(getApplicationContext(), this);
		telephonyManager = (TelephonyManager) getApplication().getSystemService(Context.TELEPHONY_SERVICE);
		signalStrengthListener = new SignalStrengthListener(getApplicationContext(), this);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		isOnSchedule = intent.getBooleanExtra("isOnSchedule", false);
		if (isOnSchedule) {
			RecurringNotification.notify(getApplicationContext());
		}
		telephonyManager.listen(signalStrengthListener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
		return super.onStartCommand(intent, flags, startId);
	}

	@Nullable
	@Override
	public IBinder onBind(Intent intent) {
		Log.i(this.getClass().getSimpleName(), "onBind()");
		return null;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		telephonyManager.listen(signalStrengthListener, PhoneStateListener.LISTEN_NONE);
		Log.i(this.getClass().getSimpleName(), "onDestroy()");
	}

	public static ComponentName startService(Context context) {
		Intent foregroundServiceIntent = new Intent(context, SignalStrengthForegroundNotificationService.class);
		return context.startService(foregroundServiceIntent);
	}

	public static boolean stopService(Context context) {
		Intent foregroundServiceIntent = new Intent(context, SignalStrengthForegroundNotificationService.class);
		return context.stopService(foregroundServiceIntent);
	}

}
