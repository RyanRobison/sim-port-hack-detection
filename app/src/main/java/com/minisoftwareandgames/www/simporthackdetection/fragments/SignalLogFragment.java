package com.minisoftwareandgames.www.simporthackdetection.fragments;

import android.os.Bundle;
import android.view.*;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.minisoftwareandgames.www.simporthackdetection.*;
import com.minisoftwareandgames.www.simporthackdetection.RoomDB.ApplicationDatabase;
import com.minisoftwareandgames.www.simporthackdetection.RoomDB.SignalStrengthEntry;
import org.joda.time.DateTime;

import java.util.stream.Collectors;

public class SignalLogFragment extends Fragment {

	private ApplicationDatabase applicationDatabase;
	private SignalStrengthLogsRecycleView recyclerView;
	private int page = 0;
	private boolean outagesOnly = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
			page = getArguments().getInt("page");
			outagesOnly = getArguments().getBoolean("outagesOnly", false);
		}
		applicationDatabase = ApplicationDatabase.getInstance(getContext());
		setHasOptionsMenu(true);
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_signal_logs, container, false);
		setupActionBar();
		recyclerView = view.findViewById(R.id.signalLogRecyclerView);
		recyclerView.setHasFixedSize(true); // improves performance when all views are the same size but causes visual issues when they're not
		recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
		recyclerView.setAdapter(new SignalStrengthLogEntryAdapter(new ViewColors(getContext())));
		recyclerView.setFloatingActionButton(view.findViewById(R.id.downFab));
		return view;
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		applicationDatabase.signalStrengthsDao()
				.findSignalLogsBetweenDatesLiveData(
						DateTime.now().toLocalDate().toDateTimeAtStartOfDay().minusDays(page),
						DateTime.now().toLocalDate().toDateTimeAtStartOfDay().minusDays(page - 1))
				.observe(this, list -> {
					if (outagesOnly) {
						list = list.stream()
								.filter(SignalStrengthEntry::isZeroLevelStrength)
								.collect(Collectors.toList());
					}
					recyclerView.handleNewEntries(list);
				});
	}

	private void setupActionBar() {
		ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
		actionBar.setTitle(DateTime.now().toLocalDate().toDateTimeAtStartOfDay().minusDays(page).toString("EEEE, MMM d"));
	}

	@Override
	public void onPrepareOptionsMenu(@NonNull Menu menu) {
		MenuItem today = menu.findItem(R.id.go_to_today_item);
		if (today != null) today.setVisible(false);
	}

}
