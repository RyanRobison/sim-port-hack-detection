package com.minisoftwareandgames.www.simporthackdetection.notifications;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import androidx.core.app.NotificationCompat;
import com.minisoftwareandgames.www.simporthackdetection.R;

public class RecurringNotification {

	public static final String NOTIFICATION_TAG = "Recurring";

	public static void notify(final Context context) {
		final NotificationCompat.Builder builder = new NotificationCompat.Builder(context, NOTIFICATION_TAG)
				.setDefaults(Notification.DEFAULT_ALL)
				.setSmallIcon(R.drawable.ic_sim)
				.setPriority(NotificationCompat.PRIORITY_DEFAULT);
		notify(context, builder.build());
	}

	@TargetApi(Build.VERSION_CODES.ECLAIR)
	private static void notify(final Context context, final Notification notification) {
		final NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		nm.notify(NOTIFICATION_TAG, 0, notification);
		Log.i(RecurringNotification.class.getSimpleName(), "notify()");
	}

	/**
	 * Cancels any notifications of this type previously shown using
	 * {@link #notify(Context)}.
	 */
	@TargetApi(Build.VERSION_CODES.ECLAIR)
	public static void cancel(final Context context) {
		final NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		nm.cancel(NOTIFICATION_TAG, 0);
		Log.i(RecurringNotification.class.getSimpleName(), "cancel()");
	}
}
