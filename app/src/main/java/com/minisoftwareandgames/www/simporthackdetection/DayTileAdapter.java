package com.minisoftwareandgames.www.simporthackdetection;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import org.apache.commons.math3.stat.descriptive.rank.Percentile;
import org.joda.time.DateTime;

import java.util.*;

public class DayTileAdapter extends RecyclerView.Adapter<DayTileAdapter.MyViewHolder> {

	private Context context;
	private List<Map.Entry<DateTime, Integer>> dayCountsWithOutsides;
	private ViewColors viewColors;
	private Percentile percentile = new Percentile();
	private ViewPager viewPager;

	public DayTileAdapter(Context context, LinkedHashMap<DateTime, Integer> outagesMap, ViewColors viewColors, ViewPager viewPager) {
		this.context = context;
		this.dayCountsWithOutsides = addMarginEntriesAndOffsetToCorrectDays(outagesMap);
		this.percentile.setData(outagesMap.entrySet().stream()
				.filter(Objects::nonNull)
				.mapToDouble(entry -> (double) entry.getValue())
				.filter(value -> value != 0)
				.sorted()
				.toArray());
		this.viewColors = viewColors;
		this.viewPager = viewPager;
	}

	public static List<Map.Entry<DateTime, Integer>> addMarginEntriesAndOffsetToCorrectDays(LinkedHashMap<DateTime, Integer> map) {
		List<Map.Entry<DateTime, Integer>> list = new ArrayList<>();
		int index = 0;
		for (Map.Entry<DateTime, Integer> outagesDate : map.entrySet()) {
			if (index == 0) {
				while (index < outagesDate.getKey().getDayOfWeek()) {
					if (index % 7 == 0) {
						list.add(null);
						list.add(null);
					} else {
						list.add(null);
					}
					index++;
				}
			} else if (index % 7 == 0) {
				list.add(null);
				list.add(null);
			}
			list.add(outagesDate);
			index++;
		}
		return list;
	}

	static class MyViewHolder extends RecyclerView.ViewHolder {
		TextView textView;

		MyViewHolder(TextView textView) {
			super(textView);
			this.textView = textView;
		}
	}

	@NonNull
	@Override
	public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		TextView textView = (TextView) LayoutInflater.from(parent.getContext())
				.inflate(R.layout.day_tile, parent, false);
		return new MyViewHolder(textView);
	}

	@Override
	public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
		holder.textView.setOnClickListener(view -> {
//			Toast.makeText(context, "arr[" + position + "]: " + dayCountsWithOutsides.get(position), Toast.LENGTH_SHORT).show();
			Integer indexPosition = getFragmentPositionInPageView(dayCountsWithOutsides, position);
			if (indexPosition != null) {
				viewPager.setCurrentItem(indexPosition - 2, true);
			}
		});
		Map.Entry<DateTime, Integer> outages = dayCountsWithOutsides.get(position);
		if (outages != null && outages.getKey().getDayOfMonth() == 1) {
			// Add border to indicate first day of the month
		}
		if (position % 9 == 0 || position % 9 == 8) {
			// set month column
			String label = "";
			if (position % 9 == 0) {
				for (int i = 1; i < 8 && position + i < dayCountsWithOutsides.size(); i++) {
					Map.Entry<DateTime, Integer> entry = dayCountsWithOutsides.get(position + i);
					if (entry != null && entry.getKey() != null && entry.getKey().getDayOfMonth() == 1) {
						label = entry.getKey().toString("MMM").toUpperCase();
						break;
					}
				}
			}
			holder.textView.setText(label);
			holder.textView.setBackgroundColor(context.getColor(android.R.color.transparent));
		} else {
			if (outages != null) {
//				holder.textView.setText(String.valueOf(outages.getValue()));
				holder.textView.setText("");
				holder.textView.setBackgroundColor(viewColors.getSummaryHeatColor(percentile, outages.getValue()));
			} else {
				holder.textView.setText("");
				holder.textView.setBackgroundColor(context.getColor(android.R.color.transparent));
			}
		}
	}

	@Override
	public int getItemCount() {
		return dayCountsWithOutsides.size();
	}

	public static Integer getFragmentPositionInPageView(List<Map.Entry<DateTime, Integer>> list, int position) {
		if (list == null || list.isEmpty()) return null;
		int fragmentPosition = -1;
		boolean lastWasNull = false;
		for (int i = 0; i <= position; i++) {
			if (list.get(i) != null) {
				fragmentPosition++;
				lastWasNull = false;
			} else {
				lastWasNull = true;
			}
		}
		return fragmentPosition < 0 || lastWasNull ? null : fragmentPosition;
	}

}
