package com.minisoftwareandgames.www.simporthackdetection.fragments;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.*;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import com.minisoftwareandgames.www.simporthackdetection.ClockView;
import com.minisoftwareandgames.www.simporthackdetection.R;
import com.minisoftwareandgames.www.simporthackdetection.RoomDB.ApplicationDatabase;
import com.minisoftwareandgames.www.simporthackdetection.RoomDB.SignalStrengthEntry;
import com.minisoftwareandgames.www.simporthackdetection.ViewModels.DailyDashboardViewModel;
import org.joda.time.DateTime;

import static com.minisoftwareandgames.www.simporthackdetection.fragments.SettingsFragment.MONITOR;

public class DailyDashboardFragment extends Fragment {

	private ClockView clockView;
	private RelativeLayout outagesRelativeLayout;
	private RelativeLayout logsRelativeLayout;
	private TextView header;
	private TextView outagesTextView;
	private TextView logs;
	private String no;
	private String outage;
	private String outages;

	private DailyDashboardViewModel viewModel;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		no = getString(R.string.no);
		outage = getString(R.string.outage);
		outages = getString(R.string.outages);
		int page = 0;
		if (getArguments() != null) {
			page = getArguments().getInt("page");
		}
		viewModel = ViewModelProviders.of(this).get(DailyDashboardViewModel.class);
		viewModel.setPage(page);
		viewModel.setApplicationDatabase(ApplicationDatabase.getInstance(getContext()));
		if (viewModel.getPage() == 0 /* today */) {
			viewModel.getDailyDashboardLiveData().observe(this, dailyDashboardData -> {
				clockView.handleNewEntries(
						dailyDashboardData.getSignalStrengthEntries(),
						dailyDashboardData.getApplicationServicesChangedEvents());
				viewModel.setOutagesCount((int) dailyDashboardData.getSignalStrengthEntries().stream()
						.filter(SignalStrengthEntry::isZeroLevelStrength).count());
				outagesTextView.setText(viewModel.getOutagesText(no, outage, outages));
			});
		} else /* past days */ {
			viewModel.setOutagesCount((int) viewModel.getSignalStrengthEntries().stream()
					.filter(SignalStrengthEntry::isZeroLevelStrength).count());
		}
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_daily_dashboard, container, false);
		header = rootView.findViewById(R.id.day_header);
		header.setText(viewModel.getPage() == 0
				? getString(R.string.today_glance)
				: DateTime.now().minusDays(viewModel.getPage()).toString("EEEE, MMM d"));

		clockView = rootView.findViewById(R.id.clock_view);

		outagesRelativeLayout = rootView.findViewById(R.id.outages_relative_layout);
		outagesRelativeLayout.setOnClickListener(view -> {
			if (viewModel.getOutagesCount() > 0) {
				SignalLogFragment signalLogFragment = new SignalLogFragment();
				Bundle outagesOnlyBundle = new Bundle(viewModel.getPageBundle());
				outagesOnlyBundle.putBoolean("outagesOnly", true);
				signalLogFragment.setArguments(outagesOnlyBundle);
				getParentFragment().getFragmentManager().beginTransaction()
						.replace(R.id.fragment, signalLogFragment, SignalLogFragment.class.getSimpleName())
						.addToBackStack(SignalLogFragment.class.getSimpleName())
						.commit();
			}
		});
		outagesTextView = rootView.findViewById(R.id.outages);

		logsRelativeLayout = rootView.findViewById(R.id.logs_relative_layout);
		logsRelativeLayout.setOnClickListener(view -> {
			SignalLogFragment signalLogFragment = new SignalLogFragment();
			signalLogFragment.setArguments(viewModel.getPageBundle());
			getParentFragment().getFragmentManager().beginTransaction()
					.replace(R.id.fragment, signalLogFragment, SignalLogFragment.class.getSimpleName())
					.addToBackStack(SignalLogFragment.class.getSimpleName())
					.commit();
		});
		logs = rootView.findViewById(R.id.logs);

		if (viewModel.getPage() != 0 || !PreferenceManager.getDefaultSharedPreferences(getContext()).getBoolean(MONITOR, false)) {
			clockView.handleNewEntries(viewModel.getSignalStrengthEntries(), viewModel.getApplicationServicesChangedEvents());
			outagesTextView.setText(viewModel.getOutagesText(no, outage, outages));
		}

		return rootView;
	}

	@Override
	public void onPrepareOptionsMenu(@NonNull Menu menu) {
		Bundle bundle = getArguments();
		if (bundle != null && bundle.getInt("page", -1) == 0) {
			MenuItem today = menu.findItem(R.id.go_to_today_item);
			today.setVisible(false);
		}
	}

}
