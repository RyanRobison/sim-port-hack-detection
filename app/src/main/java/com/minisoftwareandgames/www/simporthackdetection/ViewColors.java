package com.minisoftwareandgames.www.simporthackdetection;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Paint;
import android.util.TypedValue;
import org.apache.commons.math3.stat.descriptive.rank.Percentile;

public class ViewColors {

	private int green;
	private int lightGreen;
	private int yellow;
	private int orange;
	private int red;
	private int background;

	private Paint greenPaint;
	private Paint lightGreenPaint;
	private Paint yellowPaint;
	private Paint orangePaint;
	private Paint redPaint;
	private Paint backgroundPaint;

	public ViewColors(Context context) {
		Resources.Theme theme = context.getTheme();

		green = getColorFromAttribute(R.attr.clockViewColorFour, theme);
		lightGreen = getColorFromAttribute(R.attr.clockViewColorThree, theme);
		yellow = getColorFromAttribute(R.attr.clockViewColorTwo, theme);
		orange = getColorFromAttribute(R.attr.clockViewColorOne, theme);
		red = getColorFromAttribute(R.attr.clockViewColorZero, theme);
		background = getColorFromAttribute(R.attr.backgroundColor, theme);

		greenPaint = new Paint();
		greenPaint.setColor(green);

		lightGreenPaint = new Paint();
		lightGreenPaint.setColor(lightGreen);

		yellowPaint = new Paint();
		yellowPaint.setColor(yellow);

		orangePaint = new Paint();
		orangePaint.setColor(orange);

		redPaint = new Paint();
		redPaint.setColor(red);

		backgroundPaint = new Paint();
		backgroundPaint.setColor(background);
	}

	public int getColor(int level) {
		if (level == 0) {
			return red;
		} else if (level == 1) {
			return orange;
		} else if (level == 2) {
			return yellow;
		} else if (level == 3) {
			return lightGreen;
		} else {
			return green;
		}
	}

	public Paint getPaint(int level) {
		if (level == 0) {
			return redPaint;
		} else if (level == 1) {
			return orangePaint;
		} else if (level == 2) {
			return yellowPaint;
		} else if (level == 3) {
			return lightGreenPaint;
		} else {
			return greenPaint;
		}
	}

	public static int getColorFromAttribute(int attribute, Resources.Theme theme) {
		TypedValue typedValue = new TypedValue();
		theme.resolveAttribute(attribute, typedValue, true);
		return typedValue.data;
	}

	public Paint getBackgroundPaint() {
		return backgroundPaint;
	}

	public int getSummaryHeatColor(Percentile percentile, int level) {
		if (level == 0) {
			return green;
		} else {
			if (level <= percentile.evaluate(10)) {
				return lightGreen;
			} else if (level <= percentile.evaluate(35)) {
				return yellow;
			} else if (level <= percentile.evaluate(70)) {
				return orange;
			} else {
				return red;
			}
		}
	}
}
