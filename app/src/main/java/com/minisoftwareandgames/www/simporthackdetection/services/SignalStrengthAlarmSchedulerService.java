package com.minisoftwareandgames.www.simporthackdetection.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import androidx.annotation.Nullable;
import com.minisoftwareandgames.www.simporthackdetection.broadcast_receivers.AlarmReceiver;

public class SignalStrengthAlarmSchedulerService extends Service {

	private AlarmManager alarmManager;
	private Handler mHandler = new Handler();
	private Runnable mHandlerTask = new Runnable() {
		@Override
		public void run() {
			Intent alarmReceiverIntent = new Intent(SignalStrengthAlarmSchedulerService.this, AlarmReceiver.class);
			PendingIntent pendingIntent = PendingIntent.getBroadcast(SignalStrengthAlarmSchedulerService.this, 0, alarmReceiverIntent, 0);
			alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, 0, pendingIntent);
			mHandler.postDelayed(mHandlerTask, 10000);
		}
	};

	@Override
	public void onCreate() {
		alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
		super.onCreate();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		mHandlerTask.run();
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.e(getClass().getSimpleName(), "onDestroy()");
		mHandler.removeCallbacks(mHandlerTask);
	}

	@Nullable
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	public static ComponentName startService(Context context) {
		Intent alarmScheduler = new Intent(context, SignalStrengthAlarmSchedulerService.class);
		return context.startService(alarmScheduler);
	}

	public static boolean stopService(Context context) {
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		Intent alarmReceiverIntent = new Intent(context, AlarmReceiver.class);
		PendingIntent cancelServicePendingIntent = PendingIntent.getBroadcast(context, 0, alarmReceiverIntent, 0);
		alarmManager.cancel(cancelServicePendingIntent);

		Intent alarmScheduler = new Intent(context, SignalStrengthAlarmSchedulerService.class);
		return context.stopService(alarmScheduler);
	}
}
