package com.minisoftwareandgames.www.simporthackdetection.RoomDB;

import org.joda.time.DateTime;
import org.joda.time.DateTimeComparator;

import java.util.ArrayList;
import java.util.List;

public class JoinedType {

	public SignalStrengthEntry signalStrengthEntry = null;
	public ApplicationServicesChangedEvent applicationServicesChangedEvent = null;

	public JoinedType(Object object) {
		this.signalStrengthEntry = object instanceof SignalStrengthEntry ? (SignalStrengthEntry) object : null;
		this.applicationServicesChangedEvent = object instanceof ApplicationServicesChangedEvent ? (ApplicationServicesChangedEvent) object : null;
	}

	public static List<JoinedType> joinAndSortTypes(List<SignalStrengthEntry> signalStrengthEntryList, List<ApplicationServicesChangedEvent> applicationServicesChangedEvents) {
		List<JoinedType> joinedTypes = new ArrayList<>();
		signalStrengthEntryList.forEach(signalStrengthEntry -> joinedTypes.add(new JoinedType(signalStrengthEntry)));
		applicationServicesChangedEvents.forEach(event -> joinedTypes.add(new JoinedType(event)));
		joinedTypes.sort(JoinedType::compareDateTimes);
		return joinedTypes;
	}

	public static int compareDateTimes(JoinedType first, JoinedType second) {
		DateTime one = first.signalStrengthEntry == null
				? first.applicationServicesChangedEvent.datetime
				: first.signalStrengthEntry.dateTime;
		DateTime two = second.signalStrengthEntry == null
				? second.applicationServicesChangedEvent.datetime
				: second.signalStrengthEntry.dateTime;
		return DateTimeComparator.getInstance().compare(one, two);
	}

	@Override
	public String toString() {
		String signalStrengthEntryStr = signalStrengthEntry == null ? "null" : signalStrengthEntry.toString();
		String applicationServicesChangedEventStr = applicationServicesChangedEvent == null ? "null" : applicationServicesChangedEvent.toString();
		return String.format("{signalStrengthEntry: %s, applicationServicesChangedEvent: %s}", signalStrengthEntryStr, applicationServicesChangedEventStr);
	}

}
