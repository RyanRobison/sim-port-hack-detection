package com.minisoftwareandgames.www.simporthackdetection.broadcast_receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.minisoftwareandgames.www.simporthackdetection.services.SignalStrengthForegroundNotificationService;

public class AlarmReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.i(this.getClass().getSimpleName(), "onReceive");
		Intent foregroundServiceIntent = new Intent(context, SignalStrengthForegroundNotificationService.class);
		foregroundServiceIntent.putExtra("isOnSchedule", true);
		context.startService(foregroundServiceIntent);
	}
}
