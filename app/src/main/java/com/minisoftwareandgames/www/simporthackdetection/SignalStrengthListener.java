package com.minisoftwareandgames.www.simporthackdetection;

import android.content.Context;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.util.Log;
import com.minisoftwareandgames.www.simporthackdetection.RoomDB.ApplicationDatabase;
import com.minisoftwareandgames.www.simporthackdetection.RoomDB.SignalStrengthEntry;
import com.minisoftwareandgames.www.simporthackdetection.notifications.LostServiceNotification;
import com.minisoftwareandgames.www.simporthackdetection.notifications.RecurringNotification;
import com.minisoftwareandgames.www.simporthackdetection.notifications.SimStatusNotification;
import com.minisoftwareandgames.www.simporthackdetection.services.SignalStrengthForegroundNotificationService;
import org.joda.time.DateTime;

import java.util.Collections;

public class SignalStrengthListener extends PhoneStateListener {

	private Context context;
	private ApplicationDatabase applicationDatabase;
	private SignalStrengthForegroundNotificationService service;

	public SignalStrengthListener(Context context, SignalStrengthForegroundNotificationService service) {
		this.context = context;
		this.applicationDatabase = ApplicationDatabase.getInstance(context);
		this.service = service;
	}

	@Override
	public void onSignalStrengthsChanged(SignalStrength signalStrength) {
		super.onSignalStrengthsChanged(signalStrength);
		Log.i(this.getClass().getSimpleName(), "onSignalStrengthsChanged: " + signalStrength.getLevel());
		SignalStrengthEntry signalStrengthEntry = new SignalStrengthEntry(signalStrength.getLevel(), DateTime.now());
		if (signalStrengthEntry.signalStrengthLevel == 0) {
			LostServiceNotification.notify(context, signalStrengthEntry);
		} else {
			LostServiceNotification.inactivateLossOfService();
		}
		applicationDatabase.signalStrengthsDao().insertLogEntries(signalStrengthEntry);
		SimStatusNotification.updateSignalStrengthStatus(Collections.singletonList(signalStrengthEntry), service);
		if (service.isOnSchedule()) {
			RecurringNotification.cancel(context);
			service.setOnSchedule(false);
		}
	}
}
