package com.minisoftwareandgames.www.simporthackdetection.RoomDB;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import org.joda.time.DateTime;

@Entity(tableName = "application_services_changed_events")
public class ApplicationServicesChangedEvent {
	@PrimaryKey(autoGenerate = true)
	public long uid;

	@ColumnInfo(name = "datetime")
	public DateTime datetime;

	@ColumnInfo(name = "applicationServicesChangedEventType")
	public ApplicationServicesChangedEventType applicationServicesChangedEventType;

	public ApplicationServicesChangedEvent(DateTime datetime, ApplicationServicesChangedEventType applicationServicesChangedEventType) {
		this.datetime = datetime;
		this.applicationServicesChangedEventType = applicationServicesChangedEventType;
	}

	@Override
	public String toString() {
		return String.format("{type: %s, datetime: %s}", applicationServicesChangedEventType, datetime);
	}

	public enum ApplicationServicesChangedEventType {
		DEVICE_OFF, DEVICE_ON, PREFERENCE_OFF, PREFERENCE_ON;

		public static String DEVICE_OFF_TAG = "device_off";
		public static String DEVICE_ON_TAG = "device_on";
		public static String PREFERENCE_OFF_TAG = "preference_off";
		public static String PREFERENCE_ON_TAG = "preference_on";

		public static ApplicationServicesChangedEventType fromString(String applicationServicesChangedEventType) {
			if (DEVICE_OFF_TAG.equalsIgnoreCase(applicationServicesChangedEventType)) {
				return DEVICE_OFF;
			} else if (DEVICE_ON_TAG.equalsIgnoreCase(applicationServicesChangedEventType)) {
				return DEVICE_ON;
			} else if (PREFERENCE_OFF_TAG.equalsIgnoreCase(applicationServicesChangedEventType)) {
				return PREFERENCE_OFF;
			} else if (PREFERENCE_ON_TAG.equalsIgnoreCase(applicationServicesChangedEventType)) {
				return PREFERENCE_ON;
			} else {
				return null;
			}
		}

		public boolean isOffEventType() {
			if (this == DEVICE_OFF || this == PREFERENCE_OFF) {
				return true;
			} else if (this == DEVICE_ON || this == PREFERENCE_ON) {
				return false;
			} else {
				return false;
			}
		}

		@Override
		public String toString() {
			if (this == DEVICE_OFF) {
				return DEVICE_OFF_TAG;
			} else if (this == DEVICE_ON) {
				return DEVICE_ON_TAG;
			} else if (this == PREFERENCE_OFF) {
				return PREFERENCE_OFF_TAG;
			} else if (this == PREFERENCE_ON) {
				return PREFERENCE_ON_TAG;
			} else {
				return null;
			}
		}
	}
}
