package com.minisoftwareandgames.www.simporthackdetection;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.*;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import com.minisoftwareandgames.www.simporthackdetection.RoomDB.ApplicationServicesChangedEvent;
import com.minisoftwareandgames.www.simporthackdetection.RoomDB.SignalStrengthEntry;

import java.util.ArrayList;
import java.util.List;

public class ClockView extends View {

	private Paint textPaint;
	private ViewColors viewColors;
	private Paint linePaint;
	private Paint transparentPaint;
	private int mViewWidth;
	private int mViewHeight;
	private int textSize;
	private int pieRadius;
	private Point pieCenterPoint;
	private Point tempPoint;
	private Point tempPointRight;
	private int lineLength;
	private float leftTextWidth;
	private float rightTextWidth;
	private float topTextHeight;
	private int lineThickness;
	private RectF cirRect;
	private Rect textRect;

	private List<ClockViewHelper> pieArrayList = new ArrayList<>();

	private Runnable animator = new Runnable() {
		@Override
		public void run() {
			boolean needNewFrame = false;
			for (ClockViewHelper pie : pieArrayList) {
				pie.update();
				if (!pie.isAtRest()) {
					needNewFrame = true;
				}
			}
			if (needNewFrame) {
				postDelayed(this, 10);
			}
			invalidate();
		}
	};

	public ClockView(Context context) {
		this(context, null);
	}

	public ClockView(Context context, AttributeSet attrs) {
		super(context, attrs);

		Resources.Theme theme = context.getTheme();
		TypedValue typedValue = new TypedValue();
		theme.resolveAttribute(R.attr.textColor, typedValue, true);
		int textColor = typedValue.data;

		viewColors = new ViewColors(context);

		textSize = sp2px(context, 15);
		lineThickness = dip2px(context, 1);
		lineLength = dip2px(context, 10);

		textPaint = new Paint();
		textPaint.setAntiAlias(true);
		textPaint.setColor(textColor);
		textPaint.setTextSize(textSize);
		textPaint.setTextAlign(Paint.Align.CENTER);
		Paint.FontMetrics fm = new Paint.FontMetrics();
		textPaint.getFontMetrics(fm);
		textRect = new Rect();
		textPaint.getTextBounds("18", 0, 1, textRect);
		linePaint = new Paint(textPaint);
		linePaint.setColor(textColor);
		linePaint.setStrokeWidth(lineThickness);
		transparentPaint = new Paint(linePaint);
		transparentPaint.setColor(Color.TRANSPARENT);
		tempPoint = new Point();
		pieCenterPoint = new Point();
		tempPointRight = new Point();
		cirRect = new RectF();
		leftTextWidth = textPaint.measureText("18");
		rightTextWidth = textPaint.measureText("06");
		topTextHeight = textRect.height();
	}

	public void setData(List<ClockViewHelper> helperList) {
		if (helperList != null && !helperList.isEmpty()) {
			int pieSize = pieArrayList.isEmpty() ? 0 : pieArrayList.size();
			for (int i = 0; i < helperList.size(); i++) {
				if (i > pieSize - 1) {
					pieArrayList.add(new ClockViewHelper(0, 0, helperList.get(i)));
				} else {
					pieArrayList.set(i, pieArrayList.get(i).setTarget(helperList.get(i)));
				}
			}
			int temp = pieArrayList.size() - helperList.size();
			for (int i = 0; i < temp; i++) {
				pieArrayList.remove(pieArrayList.size() - 1);
			}
		} else {
			pieArrayList.clear();
		}

		removeCallbacks(animator);
		post(animator);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		drawBackground(canvas);
		if (pieArrayList != null) {
			for (ClockViewHelper helper : pieArrayList) {
				canvas.drawArc(cirRect, helper.getStart(), helper.getSweep(), true, viewColors.getPaint(helper.getSignalStrength()));
			}
		}
	}

	private void drawBackground(Canvas canvas) {
		for (int i = 0; i < 12; i++) {
			tempPoint.set(pieCenterPoint.x - (int) (Math.sin(Math.PI / 12 * i) * (pieRadius + lineLength)),
					pieCenterPoint.y - (int) (Math.cos(Math.PI / 12 * i) * (pieRadius + lineLength)));
			tempPointRight.set(
					pieCenterPoint.x + (int) (Math.sin(Math.PI / 12 * i) * (pieRadius + lineLength)),
					pieCenterPoint.y + (int) (Math.cos(Math.PI / 12 * i) * (pieRadius + lineLength)));
			canvas.drawLine(tempPoint.x, tempPoint.y, tempPointRight.x, tempPointRight.y, linePaint);
		}
		canvas.drawCircle(pieCenterPoint.x, pieCenterPoint.y, pieRadius + (lineThickness * 2), linePaint);
		canvas.drawCircle(pieCenterPoint.x, pieCenterPoint.y, pieRadius + lineThickness, viewColors.getBackgroundPaint());
		canvas.drawCircle(pieCenterPoint.x, pieCenterPoint.y, pieRadius, transparentPaint);
		canvas.drawText("00", pieCenterPoint.x, topTextHeight, textPaint);
		canvas.drawText("12", pieCenterPoint.x, mViewHeight, textPaint);
		canvas.drawText("18", leftTextWidth / 2,
				pieCenterPoint.y + textRect.height() / 2, textPaint);
		canvas.drawText("06", mViewWidth - rightTextWidth / 2,
				pieCenterPoint.y + textRect.height() / 2, textPaint);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		mViewWidth = measureWidth(widthMeasureSpec);
		mViewHeight = measureHeight(heightMeasureSpec);
		pieRadius = mViewWidth / 2 - lineLength * 2 - (int) (textPaint.measureText("18") / 2);
		pieCenterPoint.set(mViewWidth / 2 - (int) rightTextWidth / 2 + (int) leftTextWidth / 2,
				mViewHeight / 2 + textSize / 2 - (int) (textPaint.measureText("18") / 2));
		cirRect.set(pieCenterPoint.x - pieRadius - (lineThickness * 2),
				pieCenterPoint.y - pieRadius - (lineThickness * 2),
				pieCenterPoint.x + pieRadius + (lineThickness * 2),
				pieCenterPoint.y + pieRadius + (lineThickness * 2));
		setMeasuredDimension(mViewWidth, mViewHeight);
	}

	private int measureWidth(int measureSpec) {
		int preferred = 3;
		return getMeasurement(measureSpec, preferred);
	}

	private int measureHeight(int measureSpec) {
		int preferred = mViewWidth;
		return getMeasurement(measureSpec, preferred);
	}

	private int getMeasurement(int measureSpec, int preferred) {
		int specSize = View.MeasureSpec.getSize(measureSpec);
		int measurement;

		switch (View.MeasureSpec.getMode(measureSpec)) {
			case View.MeasureSpec.EXACTLY:
				measurement = specSize;
				break;
			case View.MeasureSpec.AT_MOST:
				measurement = Math.min(preferred, specSize);
				break;
			default:
				measurement = preferred;
				break;
		}
		return measurement;
	}

	public static int dip2px(Context context, float dipValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dipValue * scale + 0.5f);
	}

	public static int px2dip(Context context, float pxValue) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (pxValue / scale + 0.5f);
	}

	public static int sp2px(Context context, float spValue) {
		final float fontScale = context.getResources().getDisplayMetrics().scaledDensity;
		return (int) (spValue * fontScale + 0.5f);
	}

	public void handleNewEntries(List<SignalStrengthEntry> signalStrengthEntries, List<ApplicationServicesChangedEvent> applicationServicesChangedEvents) {
		this.setData(ClockViewHelper.getHelperData(signalStrengthEntries, applicationServicesChangedEvents));
	}

}
