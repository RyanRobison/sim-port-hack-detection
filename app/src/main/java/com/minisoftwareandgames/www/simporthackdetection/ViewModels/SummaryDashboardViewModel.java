package com.minisoftwareandgames.www.simporthackdetection.ViewModels;

import androidx.lifecycle.ViewModel;
import androidx.viewpager.widget.ViewPager;
import com.minisoftwareandgames.www.simporthackdetection.RoomDB.ApplicationDatabase;
import org.joda.time.DateTime;

import java.util.LinkedHashMap;

public class SummaryDashboardViewModel extends ViewModel {

	private ApplicationDatabase applicationDatabase;
	private LinkedHashMap<DateTime, Integer> outagesMap = new LinkedHashMap<>();
	private ViewPager viewPager;

	public void setApplicationDatabase(ApplicationDatabase applicationDatabase) {
		this.applicationDatabase = applicationDatabase;
	}

	public ApplicationDatabase getApplicationDatabase() {
		return applicationDatabase;
	}

	public LinkedHashMap<DateTime, Integer> getOutagesMap() {
		return outagesMap;
	}

	public ViewPager getViewPager() {
		return viewPager;
	}

	public void setViewPager(ViewPager viewPager) {
		this.viewPager = viewPager;
	}
}
