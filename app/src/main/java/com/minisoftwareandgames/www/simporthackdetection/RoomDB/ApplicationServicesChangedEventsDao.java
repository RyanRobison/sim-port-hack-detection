package com.minisoftwareandgames.www.simporthackdetection.RoomDB;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import org.joda.time.DateTime;

import java.util.List;

@Dao
public interface ApplicationServicesChangedEventsDao {

	@Query("SELECT * FROM application_services_changed_events WHERE datetime BETWEEN :from AND :to")
	LiveData<List<ApplicationServicesChangedEvent>> findApplicationServicesChangedEventsBetweenDatesLiveData(DateTime from, DateTime to);

	@Query("SELECT * FROM application_services_changed_events WHERE datetime BETWEEN :from AND :to")
	List<ApplicationServicesChangedEvent> findApplicationServicesChangedEventsBetweenDates(DateTime from, DateTime to);

	@Insert
	long[] insertApplicationServicesChangedEvent(ApplicationServicesChangedEvent... applicationServicesChangedEvents);
}
