package com.minisoftwareandgames.www.simporthackdetection.broadcast_receivers;

import android.content.*;
import android.util.Log;
import com.minisoftwareandgames.www.simporthackdetection.RoomDB.ApplicationDatabase;
import com.minisoftwareandgames.www.simporthackdetection.RoomDB.ApplicationServicesChangedEvent;
import com.minisoftwareandgames.www.simporthackdetection.fragments.SettingsFragment;
import com.minisoftwareandgames.www.simporthackdetection.multi_preferences.MultiPreferences;
import com.minisoftwareandgames.www.simporthackdetection.services.SignalStrengthAlarmSchedulerService;
import com.minisoftwareandgames.www.simporthackdetection.services.SignalStrengthForegroundNotificationService;
import org.joda.time.DateTime;

import static com.minisoftwareandgames.www.simporthackdetection.RoomDB.ApplicationServicesChangedEvent.ApplicationServicesChangedEventType.DEVICE_OFF;
import static com.minisoftwareandgames.www.simporthackdetection.RoomDB.ApplicationServicesChangedEvent.ApplicationServicesChangedEventType.DEVICE_ON;

public class DevicePowerStateBroadcastReceiver extends BroadcastReceiver {

	private static DevicePowerStateBroadcastReceiver devicePowerStateBroadcastReceiver;

	public static DevicePowerStateBroadcastReceiver register(Context context) {
		if (context == null) return null;
		devicePowerStateBroadcastReceiver = new DevicePowerStateBroadcastReceiver();
		context.registerReceiver(devicePowerStateBroadcastReceiver, new IntentFilter("android.intent.action.ACTION_SHUTDOWN"));
		context.registerReceiver(devicePowerStateBroadcastReceiver, new IntentFilter("android.intent.action.QUICKBOOT_POWEROFF"));
		return devicePowerStateBroadcastReceiver;
	}

	private static void unregister(Context context) {
		if (context == null) return;
		context.unregisterReceiver(devicePowerStateBroadcastReceiver);
	}

	/**
	 * The SharedPreferences for if signal strength monitoring is enabled should only ever have to handle the default
	 * case if the application is never launched between installing the app and the device being rebooted.
	 * Typically, the application is installed when the device is on and the application is launched. From that first
	 * launch, the monitoring preference will be defaulted to true or changed by the user to false.
	 *
	 * @param context The Context in which the receiver is running.
	 * @param intent  The Intent being received.
	 */
	@Override
	public void onReceive(Context context, Intent intent) {
		if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
			Log.i(DevicePowerStateBroadcastReceiver.class.getSimpleName(), "ACTION_BOOT_COMPLETED");
			MultiPreferences sharedPreferences = SettingsFragment.getPreferences(context);
			if (sharedPreferences.getBoolean(SettingsFragment.MONITOR, true)) {
				SignalStrengthForegroundNotificationService.startService(context);
				SignalStrengthAlarmSchedulerService.startService(context);
				ApplicationDatabase.getInstance(context)
						.eventsDao()
						.insertApplicationServicesChangedEvent(new ApplicationServicesChangedEvent(DateTime.now(), DEVICE_ON));
			}
		} else if (Intent.ACTION_SHUTDOWN.equals(intent.getAction())) {
			Log.i(DevicePowerStateBroadcastReceiver.class.getSimpleName(), "ACTION_SHUTDOWN");
			SignalStrengthForegroundNotificationService.stopService(context);
			SignalStrengthAlarmSchedulerService.stopService(context);
			ApplicationDatabase.getInstance(context)
					.eventsDao()
					.insertApplicationServicesChangedEvent(new ApplicationServicesChangedEvent(DateTime.now(), DEVICE_OFF));
			unregister(context);
		}
	}
}
