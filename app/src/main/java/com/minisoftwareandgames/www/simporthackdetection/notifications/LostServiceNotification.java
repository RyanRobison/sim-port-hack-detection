package com.minisoftwareandgames.www.simporthackdetection.notifications;

import android.app.Notification;
import android.content.Context;
import android.util.Log;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import com.minisoftwareandgames.www.simporthackdetection.R;
import com.minisoftwareandgames.www.simporthackdetection.RoomDB.SignalStrengthEntry;
import com.minisoftwareandgames.www.simporthackdetection.fragments.SettingsFragment;

import static androidx.core.app.NotificationCompat.PRIORITY_HIGH;
import static androidx.core.app.NotificationCompat.PRIORITY_LOW;
import static com.minisoftwareandgames.www.simporthackdetection.RoomDB.Converters.dateToNotificationString;
import static com.minisoftwareandgames.www.simporthackdetection.activites.MainActivity.getOpenAppPendingIntent;

public class LostServiceNotification {

	public static final String CHANNEL_ID = "LostService";
	public static final String CHANNEL_ID_SILENT = "SilentLostService";
	private static boolean notified = false;

	public static void notify(Context context, SignalStrengthEntry entry) {
		if (!notified) {
			boolean activeNotifications = SettingsFragment.getPreferences(context).getBoolean(SettingsFragment.ACTIVE_NOTIFICATIONS, true);
			boolean persistentNotifications = SettingsFragment.getPreferences(context).getBoolean(SettingsFragment.PERSISTENT_NOTIFICATIONS, true);
			String time = dateToNotificationString(entry.dateTime);
			int notificationId = (int) entry.uid;
			NotificationCompat.Builder builder = new NotificationCompat.Builder(context, activeNotifications ? CHANNEL_ID : CHANNEL_ID_SILENT)
					.setContentText(context.getString(R.string.lost_service_content_text, time))
					.setSmallIcon(R.drawable.ic_sim_alert)
					.setPriority(activeNotifications ? PRIORITY_HIGH : PRIORITY_LOW)
					.setCategory(Notification.CATEGORY_STATUS)
					.setAutoCancel(true)
					.setContentIntent(getOpenAppPendingIntent(context));
			NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
			if (!activeNotifications) {
				builder.setVibrate(new long[]{0L});
			}
			Notification notification = builder.build();

			if (activeNotifications && persistentNotifications) {
				notification.flags = notification.flags | Notification.FLAG_INSISTENT;
			}
			notificationManager.notify(notificationId, notification);
			notified = true;
			Log.i("notify()", String.format("active: %s, persistent: %s", activeNotifications, persistentNotifications));
		}
	}

	public static void inactivateLossOfService() {
		notified = false;
	}

}
