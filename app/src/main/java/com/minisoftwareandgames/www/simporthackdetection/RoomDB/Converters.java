package com.minisoftwareandgames.www.simporthackdetection.RoomDB;

import androidx.room.TypeConverter;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

public class Converters {

	public static String dateFormat = "HH:mm:ss EEE, MMM d, y";
	public static String notificationDateFormat = "HH:mm:ss";

	@TypeConverter
	public static DateTime fromTimestamp(Long value) {
		return value == null ? null : new DateTime(value);
	}

	@TypeConverter
	public static Long dateToTimestamp(DateTime date) {
		return date == null ? null : date.getMillis();
	}

	public static String dateToUiString(DateTime date) {
		return date.toString(DateTimeFormat.forPattern(dateFormat));
	}
	public static String dateToNotificationString(DateTime date) {
		return date.toString(DateTimeFormat.forPattern(notificationDateFormat));
	}

	@TypeConverter
	public static String applicationServicesChangedEventTypeToString(ApplicationServicesChangedEvent.ApplicationServicesChangedEventType applicationServicesChangedEventType) {
		return applicationServicesChangedEventType.toString();
	}

	@TypeConverter
	public static ApplicationServicesChangedEvent.ApplicationServicesChangedEventType applicationServicesChangedEventTypeFromString(String applicationServicesChangedEventDaoEventTypeString) {
		return ApplicationServicesChangedEvent.ApplicationServicesChangedEventType.fromString(applicationServicesChangedEventDaoEventTypeString);
	}
}
