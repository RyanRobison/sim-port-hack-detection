package com.minisoftwareandgames.www.simporthackdetection.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.viewpager.widget.ViewPager;
import com.minisoftwareandgames.www.simporthackdetection.DayTileAdapter;
import com.minisoftwareandgames.www.simporthackdetection.DayTileRecyclerView;
import com.minisoftwareandgames.www.simporthackdetection.R;
import com.minisoftwareandgames.www.simporthackdetection.RoomDB.ApplicationDatabase;
import com.minisoftwareandgames.www.simporthackdetection.RoomDB.SignalStrengthEntry;
import com.minisoftwareandgames.www.simporthackdetection.ViewColors;
import com.minisoftwareandgames.www.simporthackdetection.ViewModels.SummaryDashboardViewModel;
import org.joda.time.DateTime;
import org.joda.time.Days;

import java.util.List;

public class SummaryDashboardFragment extends Fragment {

	private SummaryDashboardViewModel summaryDashboardViewModel;
	private ViewPager viewPager;
	private DayTileRecyclerView recyclerView;

	public void setParentViewPager(ViewPager viewPager) {
		this.viewPager = viewPager;
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		summaryDashboardViewModel = ViewModelProviders.of(this).get(SummaryDashboardViewModel.class);
		summaryDashboardViewModel.setViewPager(viewPager);
		summaryDashboardViewModel.setApplicationDatabase(ApplicationDatabase.getInstance(getContext()));
		SignalStrengthEntry firstLog = summaryDashboardViewModel.getApplicationDatabase().signalStrengthsDao().getFirstSignalLog();
		DateTime firstLogDay = firstLog.dateTime.withDayOfMonth(1).withTimeAtStartOfDay();
		for (int i = 0; i <= Days.daysBetween(firstLogDay, DateTime.now()).getDays(); i++) {
			summaryDashboardViewModel.getOutagesMap().put(firstLogDay.plusDays(i), 0);
		}
		List<SignalStrengthEntry> entries = summaryDashboardViewModel.getApplicationDatabase().signalStrengthsDao().getAllOutageLogs();
		entries.forEach(entry -> summaryDashboardViewModel.getOutagesMap().put(
				entry.dateTime.withTimeAtStartOfDay(),
				summaryDashboardViewModel.getOutagesMap().getOrDefault(entry.dateTime.withTimeAtStartOfDay(), 0) + 1));
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_summary_dashboard, container, false);
		recyclerView = rootView.findViewById(R.id.dayTileRecyclerView);
		recyclerView.setHasFixedSize(true);
		recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 9));
		recyclerView.setAdapter(new DayTileAdapter(getContext(), summaryDashboardViewModel.getOutagesMap(), new ViewColors(getContext()), summaryDashboardViewModel.getViewPager()));
		return rootView;
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}
}
