package com.minisoftwareandgames.www.simporthackdetection.activites;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import com.minisoftwareandgames.www.simporthackdetection.R;
import com.minisoftwareandgames.www.simporthackdetection.RoomDB.ApplicationDatabase;
import com.minisoftwareandgames.www.simporthackdetection.RoomDB.SignalStrengthEntry;
import com.minisoftwareandgames.www.simporthackdetection.broadcast_receivers.DevicePowerStateBroadcastReceiver;
import com.minisoftwareandgames.www.simporthackdetection.fragments.DashboardViewPagerFragment;
import com.minisoftwareandgames.www.simporthackdetection.fragments.SettingsFragment;
import com.minisoftwareandgames.www.simporthackdetection.multi_preferences.MultiPreferences;
import com.minisoftwareandgames.www.simporthackdetection.notifications.LostServiceNotification;
import com.minisoftwareandgames.www.simporthackdetection.notifications.RecurringNotification;
import com.minisoftwareandgames.www.simporthackdetection.services.SignalStrengthAlarmSchedulerService;
import com.minisoftwareandgames.www.simporthackdetection.services.SignalStrengthForegroundNotificationService;
import org.joda.time.DateTime;
import org.joda.time.Days;

import static android.app.NotificationManager.*;
import static com.minisoftwareandgames.www.simporthackdetection.ViewColors.getColorFromAttribute;

public class MainActivity extends AppCompatActivity {

	private FragmentManager fragmentManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		String packageName = getPackageName();
		PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
		if (!powerManager.isIgnoringBatteryOptimizations(packageName)) {
			Intent intent = new Intent();
			intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
			intent.setData(Uri.parse("package:" + packageName));
			startActivity(intent);
		}

		setContentView(R.layout.activity_main);

		DevicePowerStateBroadcastReceiver.register(this);

		setupFragment();
		setupNotificationChannels();

		MultiPreferences sharedPreferences = SettingsFragment.getPreferences(this);
		if (sharedPreferences.getBoolean(SettingsFragment.MONITOR, true)) {
			SignalStrengthForegroundNotificationService.startService(getApplicationContext());
			SignalStrengthAlarmSchedulerService.startService(getApplicationContext());
		}

		boolean isDark = sharedPreferences.getBoolean(SettingsFragment.DARK_THEME, false);
		setTheme(isDark ? R.style.DarkTheme : R.style.LightTheme);
		AppCompatDelegate.setDefaultNightMode(isDark ? AppCompatDelegate.MODE_NIGHT_YES : AppCompatDelegate.MODE_NIGHT_NO);
		setStatusBarStyleAccordingToDarkThemeValue(isDark, this);
		setupToolbar();
	}

	public static void setStatusBarStyleAccordingToDarkThemeValue(boolean isDark, Context context) {
		final int flags = ((Activity) context).getWindow().getDecorView().getSystemUiVisibility();  // We don't want to override existing flags
		((Activity) context).getWindow().getDecorView()
				.setSystemUiVisibility(isDark
						? (flags & ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR)  // Sets the status bar to have dark text color for contrast to the background color
						: (flags | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR));  // Same as above, but light
		((Activity) context).getWindow().setStatusBarColor(getColorFromAttribute(R.attr.backgroundColor, context.getTheme()));

//		((Activity) context).getWindow().setNavigationBarColor(getColorFromAttribute(R.attr.backgroundColor, context.getTheme()));
	}

	private void setupFragment() {
		fragmentManager = getSupportFragmentManager();
		DashboardViewPagerFragment dashboardViewPagerFragment = new DashboardViewPagerFragment();
		Bundle bundle = new Bundle();
		bundle.putInt(DashboardViewPagerFragment.BUNDLE_VIEW_PAGES, daysSinceFirstInstall());
		dashboardViewPagerFragment.setArguments(bundle);
		fragmentManager.getFragments().forEach(fragment -> fragmentManager.beginTransaction().remove(fragment).commit());
		fragmentManager.beginTransaction()
				.add(R.id.fragment, dashboardViewPagerFragment, DashboardViewPagerFragment.class.getSimpleName())
				.commit();
	}

	private void setupNotificationChannels() {
		createNotificationChannel(this, IMPORTANCE_HIGH, new NotificationChannelHelper(
				R.string.lost_service_channel_name,
				R.string.lost_service_channel_description,
				LostServiceNotification.CHANNEL_ID));
		createNotificationChannel(this, IMPORTANCE_LOW, new NotificationChannelHelper(
				R.string.silent_lost_service_channel_name,
				R.string.silent_lost_service_channel_description,
				LostServiceNotification.CHANNEL_ID_SILENT));
		createNotificationChannel(this, IMPORTANCE_HIGH, new NotificationChannelHelper(
				R.string.recurring_channel_name,
				R.string.recurring_channel_description,
				RecurringNotification.NOTIFICATION_TAG));
	}

	public static PendingIntent getOpenAppPendingIntent(Context context) {
		return PendingIntent.getActivity(
				context,
				0,
				new Intent(context, MainActivity.class),
				PendingIntent.FLAG_UPDATE_CURRENT);
	}

	private void setupToolbar() {
		Toolbar myToolbar = findViewById(R.id.main_activity_toolbar);
		myToolbar.setBackgroundColor(getColorFromAttribute(R.attr.backgroundColor, getTheme()));
		myToolbar.setTitleTextColor(getColorFromAttribute(R.attr.textColor, getTheme()));
		setSupportActionBar(myToolbar);
	}

	@Override
	public void onConfigurationChanged(@NonNull Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		setupToolbar();
		setupFragment();
	}

	private int daysSinceFirstInstall() {
		int count = 1;  // include today
		SignalStrengthEntry firstEntry = ApplicationDatabase.getInstance(this).signalStrengthsDao().getFirstSignalLog();
		if (firstEntry != null) {
			count += Days.daysBetween(firstEntry.dateTime.withTimeAtStartOfDay(), DateTime.now().withTimeAtStartOfDay()).getDays();
		}
		return count;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Log.d(this.getClass().getSimpleName(), "Menu item selected: " + item.getTitle());
		if (item.getItemId() == R.id.settings_item) {
			fragmentManager.beginTransaction()
					.replace(R.id.fragment, new SettingsFragment(), SettingsFragment.class.getSimpleName())
					.addToBackStack(SettingsFragment.class.getSimpleName())
					.commit();
		}
		return super.onOptionsItemSelected(item);
	}

	private static void createNotificationChannel(Context context, int importance, NotificationChannelHelper notificationChannelHelper) {
		// Create the NotificationChannel, but only on API 26+ because
		// the NotificationChannel class is new and not in the support library
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			String name = context.getString(notificationChannelHelper.nameResourceId);
			String description = context.getString(notificationChannelHelper.descriptionResourceId);
			NotificationChannel channel = new NotificationChannel(notificationChannelHelper.id, name, importance);
			channel.setDescription(description);
			if (importance == IMPORTANCE_LOW) {
				channel.setSound(null, null);
				channel.setVibrationPattern(new long[]{0L});
			}
			// Register the channel with the system; you can't change the importance
			// or other notification behaviors after this
			NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
			notificationManager.createNotificationChannel(channel);
		}
	}

	class NotificationChannelHelper {
		int nameResourceId;
		int descriptionResourceId;
		String id;

		NotificationChannelHelper(int nameResourceId, int descriptionResourceId, String id) {
			this.nameResourceId = nameResourceId;
			this.descriptionResourceId = descriptionResourceId;
			this.id = id;
		}
	}

}
