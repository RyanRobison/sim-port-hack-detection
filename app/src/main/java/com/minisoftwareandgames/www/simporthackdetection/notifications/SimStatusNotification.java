package com.minisoftwareandgames.www.simporthackdetection.notifications;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import androidx.core.app.NotificationCompat;
import com.minisoftwareandgames.www.simporthackdetection.R;
import com.minisoftwareandgames.www.simporthackdetection.RoomDB.SignalStrengthEntry;

import java.util.List;

import static com.minisoftwareandgames.www.simporthackdetection.activites.MainActivity.getOpenAppPendingIntent;

public class SimStatusNotification {

	private static String NOTIFICATION_TAG = "SimStatus";
	private static int CHANNEL_ID = 2, OLD_SDK_CHANNEL_ID = 1;
	private static NotificationCompat.Builder notificationBuilder;

	public static synchronized void startForegroundServiceNotification(Context context, Service service) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			String PACKAGE_NAME = context.getPackageName();
			NotificationChannel chan = new NotificationChannel(PACKAGE_NAME, NOTIFICATION_TAG, NotificationManager.IMPORTANCE_LOW);
			chan.setLightColor(Color.BLUE);
			chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
			NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			manager.createNotificationChannel(chan);    // TODO: handle possible NPE

			notificationBuilder = new NotificationCompat.Builder(service, PACKAGE_NAME)
					.setSmallIcon(R.drawable.ic_sim)
					.setContentIntent(getOpenAppPendingIntent(context))
					.setPriority(NotificationManager.IMPORTANCE_MIN)
					.setCategory(Notification.CATEGORY_SERVICE)
					.setOngoing(true)
					.setOnlyAlertOnce(true); // TODO: user preference
			service.startForeground(CHANNEL_ID, notificationBuilder.build());
		} else {
			service.startForeground(OLD_SDK_CHANNEL_ID, new Notification());
		}
	}

	/**
	 * When the status changes, update the icon (if dropped to 0) and subtext with status
	 */
	public static void updateSignalStrengthStatus(List<SignalStrengthEntry> signalStrengthOverInterval, Service service) {
		if (signalStrengthOverInterval == null || signalStrengthOverInterval.size() == 0 || notificationBuilder == null) {
			return;
		}
		String[] SIGNAL_STRENGTH_NAMES = {"None", "Poor", "Moderate", "Good", "Great"};

		SignalStrengthEntry minStrengthEntry = signalStrengthOverInterval.stream()
				.min(SignalStrengthEntry::compareSignalStrengths /* Optional case handled with null or empty list condition */)
				.get();
		int minStrength = minStrengthEntry.signalStrengthLevel;
		notificationBuilder.setSmallIcon(minStrength == 0 ? R.drawable.ic_sim_alert : R.drawable.ic_sim);
		notificationBuilder.setWhen(minStrengthEntry.dateTime.getMillis());
		if (minStrength > 0 && minStrength <= 4) {
			notificationBuilder.setSubText(SIGNAL_STRENGTH_NAMES[minStrength]);
		} else {
			// According to SignalStrength, -1 is valid signal strength value
			notificationBuilder.setSubText(SIGNAL_STRENGTH_NAMES[0]);
		}
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			service.startForeground(CHANNEL_ID, notificationBuilder.build());
		} else {
			service.startForeground(OLD_SDK_CHANNEL_ID, new Notification());
		}
	}

}
