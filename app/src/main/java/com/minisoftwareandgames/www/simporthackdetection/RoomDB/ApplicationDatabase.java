package com.minisoftwareandgames.www.simporthackdetection.RoomDB;

import android.content.Context;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import java.io.File;

@Database(
		version = 2,
		entities = {
			SignalStrengthEntry.class,
			ApplicationServicesChangedEvent.class
		}
)
@TypeConverters({Converters.class})
public abstract class ApplicationDatabase extends RoomDatabase {
	private static ApplicationDatabase INSTANCE;
	public abstract ApplicationServicesChangedEventsDao eventsDao();
	public abstract SignalStrengthsDao signalStrengthsDao();
	private static final Object sLock = new Object();

	public static ApplicationDatabase getInstance(Context context) {
		synchronized (sLock) {
			if (INSTANCE == null) {
				String oldDbName = "signal-strength-logs.db";
				String newDbName = "application.db";
				File oldDatabaseFile = context.getDatabasePath(oldDbName);
				if (oldDatabaseFile != null) {
					File newDatabaseFile = new File(oldDatabaseFile.getParentFile(), newDbName);
					oldDatabaseFile.renameTo(newDatabaseFile);
				}
				INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
						ApplicationDatabase.class, newDbName)
						.addMigrations(Migrations.getAllMigrations())
						.allowMainThreadQueries()
						.enableMultiInstanceInvalidation()  // allows for synchronization between processes
						.build();
			}
			return INSTANCE;
		}
	}
}
