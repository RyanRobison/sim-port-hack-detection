package com.minisoftwareandgames.www.simporthackdetection.ViewModels;

import android.os.Bundle;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.ViewModel;
import com.minisoftwareandgames.www.simporthackdetection.RoomDB.ApplicationDatabase;
import com.minisoftwareandgames.www.simporthackdetection.RoomDB.ApplicationServicesChangedEvent;
import com.minisoftwareandgames.www.simporthackdetection.RoomDB.SignalStrengthEntry;
import org.joda.time.DateTime;

import java.util.List;

import static java.util.Collections.emptyList;

public class DailyDashboardViewModel extends ViewModel {

	private ApplicationDatabase applicationDatabase = null;
	private MediatorLiveData<DailyDashboardData> mediatorLiveData = null;
	private LiveData<List<SignalStrengthEntry>> liveSignalStrengthEntries = null;
	private List<SignalStrengthEntry> signalStrengthEntries = null;
	private LiveData<List<ApplicationServicesChangedEvent>> liveApplicationServicesChangedEvents = null;
	private List<ApplicationServicesChangedEvent> applicationServicesChangedEvents = null;
	private int page = 0;
	private int outagesCount = 0;

	public void setApplicationDatabase(ApplicationDatabase applicationDatabase) {
		this.applicationDatabase = applicationDatabase;
	}

	public MediatorLiveData<DailyDashboardData> getDailyDashboardLiveData() {
		if (mediatorLiveData == null) {
			mediatorLiveData = new MediatorLiveData<>();
			mediatorLiveData.addSource(getLiveSignalStrengthEntries(), signalStrengthEntryList ->
					mediatorLiveData.setValue(new DailyDashboardData(
							getLiveSignalStrengthEntries().getValue(),
							getLiveApplicationServicesChangedEvents().getValue())));
			mediatorLiveData.addSource(getLiveApplicationServicesChangedEvents(), applicationServicesChangedEvents ->
					mediatorLiveData.setValue(new DailyDashboardData(
							getLiveSignalStrengthEntries().getValue(),
							getLiveApplicationServicesChangedEvents().getValue())));
		}
		return mediatorLiveData;
	}

	private LiveData<List<SignalStrengthEntry>> getLiveSignalStrengthEntries() {
		if (liveSignalStrengthEntries == null) {
			liveSignalStrengthEntries = applicationDatabase.signalStrengthsDao()
					.findSignalLogsBetweenDatesLiveData(
							DateTime.now().toLocalDate().toDateTimeAtStartOfDay().minusDays(page),
							DateTime.now().toLocalDate().toDateTimeAtStartOfDay().minusDays(page - 1));
		}
		return liveSignalStrengthEntries;
	}

	public List<SignalStrengthEntry> getSignalStrengthEntries() {
		if (signalStrengthEntries == null) {
			signalStrengthEntries = applicationDatabase.signalStrengthsDao()
					.findSignalLogsBetweenDates(
							DateTime.now().toLocalDate().toDateTimeAtStartOfDay().minusDays(page),
							DateTime.now().toLocalDate().toDateTimeAtStartOfDay().minusDays(page - 1));
		}
		return signalStrengthEntries;
	}

	private LiveData<List<ApplicationServicesChangedEvent>> getLiveApplicationServicesChangedEvents() {
		if (liveApplicationServicesChangedEvents == null) {
			liveApplicationServicesChangedEvents = applicationDatabase.eventsDao()
					.findApplicationServicesChangedEventsBetweenDatesLiveData(
							DateTime.now().toLocalDate().toDateTimeAtStartOfDay().minusDays(page),
							DateTime.now().toLocalDate().toDateTimeAtStartOfDay().minusDays(page - 1));
		}
		return liveApplicationServicesChangedEvents;
	}

	public List<ApplicationServicesChangedEvent> getApplicationServicesChangedEvents() {
		if (applicationServicesChangedEvents == null) {
			applicationServicesChangedEvents = applicationDatabase.eventsDao()
					.findApplicationServicesChangedEventsBetweenDates(
							DateTime.now().toLocalDate().toDateTimeAtStartOfDay().minusDays(page),
							DateTime.now().toLocalDate().toDateTimeAtStartOfDay().minusDays(page - 1));
		}
		return applicationServicesChangedEvents;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getOutagesCount() {
		return outagesCount;
	}

	public void setOutagesCount(int outagesCount) {
		this.outagesCount = outagesCount;
	}

	public Bundle getPageBundle() {
		Bundle pageBundle = new Bundle();
		pageBundle.putInt("page", page);
		return pageBundle;
	}

	public String getOutagesText(String no, String outage, String outages) {
		if (outagesCount == 0) {
			return String.format("%s %s", no, outages);
		} else if (outagesCount == 1) {
			return String.format("%s %s", outagesCount, outage);
		} else {
			return String.format("%s %s", outagesCount, outages);
		}
	}

	public class DailyDashboardData {

		private List<SignalStrengthEntry> signalStrengthEntries;
		private List<ApplicationServicesChangedEvent> applicationServicesChangedEvents;

		public DailyDashboardData(List<SignalStrengthEntry> signalStrengthEntries, List<ApplicationServicesChangedEvent> applicationServicesChangedEvents) {
			this.signalStrengthEntries = signalStrengthEntries;
			this.applicationServicesChangedEvents = applicationServicesChangedEvents;
		}

		public List<SignalStrengthEntry> getSignalStrengthEntries() {
			return signalStrengthEntries == null ? emptyList() : signalStrengthEntries;
		}

		public List<ApplicationServicesChangedEvent> getApplicationServicesChangedEvents() {
			return applicationServicesChangedEvents == null ? emptyList() : applicationServicesChangedEvents;
		}
	}
}
