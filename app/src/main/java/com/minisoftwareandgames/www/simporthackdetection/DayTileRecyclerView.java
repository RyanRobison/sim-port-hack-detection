package com.minisoftwareandgames.www.simporthackdetection;

import android.content.Context;
import android.util.AttributeSet;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

public class DayTileRecyclerView extends RecyclerView {

	public DayTileRecyclerView(@NonNull Context context) {
		super(context);
	}

	public DayTileRecyclerView(@NonNull Context context, @Nullable AttributeSet attrs) {
		super(context, attrs);
	}

	public DayTileRecyclerView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

}
