package com.minisoftwareandgames.www.simporthackdetection;

import org.joda.time.DateTime;
import org.junit.Test;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.minisoftwareandgames.www.simporthackdetection.DayTileAdapter.addMarginEntriesAndOffsetToCorrectDays;
import static org.junit.Assert.*;

public class DayTileTests {

	@Test
	public void testAddingMarginEntriesAndOffsetToCorrectDays() {
		// First day of the month starts on a Saturday
		DateTime day = DateTime.parse("2019-06-01");
		LinkedHashMap<DateTime, Integer> map = new LinkedHashMap<>();
		for (int i = 0; i < 200; i++) {
			map.put(day.plusDays(i), i);
		}
		List<Map.Entry<DateTime, Integer>> list = addMarginEntriesAndOffsetToCorrectDays(map);
		assertEquals(265, list.size());
		for (int i = 0; i < 11; i++) {
			Map.Entry<DateTime, Integer> entry = list.get(i);
			if (Arrays.asList(0, 1, 2, 3, 4, 5, 6, 8, 9).contains(i)) {
				assertNull(entry);
			} else {
				assertNotNull(entry);
			}
		}

		// First day of the month starts on a Wednesday
		day = DateTime.parse("2019-05-01");
		map = new LinkedHashMap<>();
		for (int i = 0; i < 200; i++) {
			map.put(day.plusDays(i), i);
		}
		list = addMarginEntriesAndOffsetToCorrectDays(map);
		assertEquals(260, list.size());
		for (int i = 0; i < 11; i++) {
			Map.Entry<DateTime, Integer> entry = list.get(i);
			if (Arrays.asList(0, 1, 2, 3, 8, 9).contains(i)) {
				assertNull(entry);
			} else {
				assertNotNull(entry);
			}
		}

		// Random day in month starts on Thursday
		day = DateTime.parse("2019-07-18");
		map = new LinkedHashMap<>();
		for (int i = 0; i < 200; i++) {
			map.put(day.plusDays(i), i);
		}
		list = addMarginEntriesAndOffsetToCorrectDays(map);
		assertEquals(263, list.size());
		for (int i = 0; i < 11; i++) {
			Map.Entry<DateTime, Integer> entry = list.get(i);
			if (Arrays.asList(0, 1, 2, 3, 4, 8, 9).contains(i)) {
				assertNull(entry);
			} else {
				assertNotNull(entry);
			}
		}
	}

	@Test
	public void getIndexOfDayBeforeMargin() {
		DateTime day = DateTime.parse("2019-07-01");    // Monday
		LinkedHashMap<DateTime, Integer> map = new LinkedHashMap<>();
		for (int i = 0; i <= 7; i++) {
			map.put(day.plusDays(i), i);
		}
		List<Map.Entry<DateTime, Integer>> list = addMarginEntriesAndOffsetToCorrectDays(map);
		assertNull(DayTileAdapter.getFragmentPositionInPageView(list, 0));
		assertNull(DayTileAdapter.getFragmentPositionInPageView(list, 1));
		assertEquals(0, DayTileAdapter.getFragmentPositionInPageView(list, 2).intValue());
		assertEquals(1, DayTileAdapter.getFragmentPositionInPageView(list, 3).intValue());
		assertEquals(2, DayTileAdapter.getFragmentPositionInPageView(list, 4).intValue());
		assertEquals(3, DayTileAdapter.getFragmentPositionInPageView(list, 5).intValue());
		assertEquals(4, DayTileAdapter.getFragmentPositionInPageView(list, 6).intValue());
		assertEquals(5, DayTileAdapter.getFragmentPositionInPageView(list, 7).intValue());
		assertNull(DayTileAdapter.getFragmentPositionInPageView(list, 8));
		assertNull(DayTileAdapter.getFragmentPositionInPageView(list, 9));

		day = DateTime.parse("2019-07-03");    // Wednesday
		map = new LinkedHashMap<>();
		for (int i = 0; i <= 7; i++) {
			map.put(day.plusDays(i), i);
		}
		list = addMarginEntriesAndOffsetToCorrectDays(map);
		assertNull(DayTileAdapter.getFragmentPositionInPageView(list, 0));
		assertNull(DayTileAdapter.getFragmentPositionInPageView(list, 1));
		assertNull(DayTileAdapter.getFragmentPositionInPageView(list, 2));
		assertNull(DayTileAdapter.getFragmentPositionInPageView(list, 3));
		assertEquals(0, DayTileAdapter.getFragmentPositionInPageView(list, 4).intValue());
		assertEquals(1, DayTileAdapter.getFragmentPositionInPageView(list, 5).intValue());
		assertEquals(2, DayTileAdapter.getFragmentPositionInPageView(list, 6).intValue());
		assertEquals(3, DayTileAdapter.getFragmentPositionInPageView(list, 7).intValue());
		assertNull(DayTileAdapter.getFragmentPositionInPageView(list, 8));
		assertNull(DayTileAdapter.getFragmentPositionInPageView(list, 9));
		assertEquals(4, DayTileAdapter.getFragmentPositionInPageView(list, 10).intValue());
		assertEquals(5, DayTileAdapter.getFragmentPositionInPageView(list, 11).intValue());
		assertEquals(6, DayTileAdapter.getFragmentPositionInPageView(list, 12).intValue());
	}

}
