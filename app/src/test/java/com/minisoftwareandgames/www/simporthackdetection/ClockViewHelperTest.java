package com.minisoftwareandgames.www.simporthackdetection;

import com.minisoftwareandgames.www.simporthackdetection.RoomDB.ApplicationServicesChangedEvent;
import com.minisoftwareandgames.www.simporthackdetection.RoomDB.SignalStrengthEntry;
import org.joda.time.DateTime;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.emptyList;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.Assert.*;

public class ClockViewHelperTest {

	@Test
	public void getHelperDataLotsOfData() {
		List<SignalStrengthEntry> signalStrengthEntries = new ArrayList<>();
		DateTime beginningOfToday = DateTime.now().withTimeAtStartOfDay();
		int seconds = 0;
		int strength = 4;
		for (int i = 0; i < 1000; i++) {
			int random = Double.valueOf(Math.random() * 100).intValue();
			if (random % 10 == 0) {
				if (strength == 1 || strength == 0) {
					strength = 4;
				} else {
					strength--;
				}
			} else if (random % 42 == 0) {
				strength = 0;
			}
			seconds += Double.valueOf(Math.random() * 1000).intValue() % 60;
			signalStrengthEntries.add(new SignalStrengthEntry(strength, beginningOfToday.plusSeconds(seconds)));
		}
		List<ClockViewHelper> clockViewHelpers = ClockViewHelper.getHelperData(signalStrengthEntries, emptyList());
		assertNotEquals("It is statistically unlinkely that the lists will be of the same size",
				signalStrengthEntries.size(),
				clockViewHelpers.size());
	}

	@Test
	public void getHelperDataWithSpecificData() {
		List<SignalStrengthEntry> signalStrengthEntries = new ArrayList<>();
		signalStrengthEntries.add(new SignalStrengthEntry(4, DateTime.parse("2019-07-04T00:00:00.000-07:00")));
		signalStrengthEntries.add(new SignalStrengthEntry(3, DateTime.parse("2019-07-04T01:00:00.000-07:00")));
		signalStrengthEntries.add(new SignalStrengthEntry(4, DateTime.parse("2019-07-04T02:00:00.000-07:00")));
		signalStrengthEntries.add(new SignalStrengthEntry(4, DateTime.parse("2019-07-04T03:00:00.000-07:00")));
		signalStrengthEntries.add(new SignalStrengthEntry(2, DateTime.parse("2019-07-04T04:00:00.000-07:00")));

		List<ClockViewHelper> clockViewHelpers = ClockViewHelper.getHelperData(signalStrengthEntries, emptyList());
		assertEquals("There should be three sections. The final entry doesn't have an \"end\" time",
				3,
				clockViewHelpers.size());
		assertThat("First data set ends on the next log item",
				clockViewHelpers.get(0),
				samePropertyValuesAs(new ClockViewHelper(
						signalStrengthEntries.get(0).dateTime.getHourOfDay(),
						signalStrengthEntries.get(0).dateTime.getMinuteOfHour(),
						signalStrengthEntries.get(0).dateTime.getSecondOfMinute(),
						signalStrengthEntries.get(1).dateTime.getHourOfDay(),
						signalStrengthEntries.get(1).dateTime.getMinuteOfHour(),
						signalStrengthEntries.get(1).dateTime.getSecondOfMinute(),
						signalStrengthEntries.get(0).signalStrengthLevel)));
		assertThat("Second data set ends on the next log item",
				clockViewHelpers.get(1),
				samePropertyValuesAs(new ClockViewHelper(
						signalStrengthEntries.get(1).dateTime.getHourOfDay(),
						signalStrengthEntries.get(1).dateTime.getMinuteOfHour(),
						signalStrengthEntries.get(1).dateTime.getSecondOfMinute(),
						signalStrengthEntries.get(2).dateTime.getHourOfDay(),
						signalStrengthEntries.get(2).dateTime.getMinuteOfHour(),
						signalStrengthEntries.get(2).dateTime.getSecondOfMinute(),
						signalStrengthEntries.get(1).signalStrengthLevel)));
		assertThat("Third data set glazes over the fourth and ends on the fifth log item",
				clockViewHelpers.get(2),
				samePropertyValuesAs(new ClockViewHelper(
						signalStrengthEntries.get(2).dateTime.getHourOfDay(),
						signalStrengthEntries.get(2).dateTime.getMinuteOfHour(),
						signalStrengthEntries.get(2).dateTime.getSecondOfMinute(),
						signalStrengthEntries.get(4).dateTime.getHourOfDay(),
						signalStrengthEntries.get(4).dateTime.getMinuteOfHour(),
						signalStrengthEntries.get(4).dateTime.getSecondOfMinute(),
						signalStrengthEntries.get(2).signalStrengthLevel)));
	}

	@Test
	public void getHelperDataWithSpecificDataThatIncludesApplicationServicesChangedEvents() {
		List<SignalStrengthEntry> signalStrengthEntries = new ArrayList<>();
		List<ApplicationServicesChangedEvent> applicationServicesChangedEvents = new ArrayList<>();
		signalStrengthEntries.add(new SignalStrengthEntry(4, DateTime.parse("2019-07-22T00:00:00.000-07:00")));

		applicationServicesChangedEvents.add(new ApplicationServicesChangedEvent(DateTime.parse("2019-07-22T00:30:00.000-07:00"), ApplicationServicesChangedEvent.ApplicationServicesChangedEventType.PREFERENCE_OFF));
		// Nothing between since services will be off until turned back on
		applicationServicesChangedEvents.add(new ApplicationServicesChangedEvent(DateTime.parse("2019-07-22T00:30:00.000-07:00"), ApplicationServicesChangedEvent.ApplicationServicesChangedEventType.PREFERENCE_ON));

		signalStrengthEntries.add(new SignalStrengthEntry(3, DateTime.parse("2019-07-22T01:00:00.000-07:00")));
		signalStrengthEntries.add(new SignalStrengthEntry(4, DateTime.parse("2019-07-22T02:00:00.000-07:00")));
		signalStrengthEntries.add(new SignalStrengthEntry(4, DateTime.parse("2019-07-22T03:00:00.000-07:00")));
		signalStrengthEntries.add(new SignalStrengthEntry(2, DateTime.parse("2019-07-22T04:00:00.000-07:00")));

		List<ClockViewHelper> clockViewHelpers = ClockViewHelper.getHelperData(signalStrengthEntries, applicationServicesChangedEvents);
		assertEquals("There should be three sections. The final entry doesn't have an \"end\" time",
				3,
				clockViewHelpers.size());
		assertThat("First data set ends when the monitoring preference is changed to off",
				clockViewHelpers.get(0),
				samePropertyValuesAs(new ClockViewHelper(
						signalStrengthEntries.get(0).dateTime.getHourOfDay(),
						signalStrengthEntries.get(0).dateTime.getMinuteOfHour(),
						signalStrengthEntries.get(0).dateTime.getSecondOfMinute(),
						applicationServicesChangedEvents.get(0).datetime.getHourOfDay(),
						applicationServicesChangedEvents.get(0).datetime.getMinuteOfHour(),
						applicationServicesChangedEvents.get(0).datetime.getSecondOfMinute(),
						signalStrengthEntries.get(0).signalStrengthLevel)));
		assertThat("Second data set begins at the first log item after the monitoring preference is changed to on and ends on the next log item after that",
				clockViewHelpers.get(1),
				samePropertyValuesAs(new ClockViewHelper(
						signalStrengthEntries.get(1).dateTime.getHourOfDay(),
						signalStrengthEntries.get(1).dateTime.getMinuteOfHour(),
						signalStrengthEntries.get(1).dateTime.getSecondOfMinute(),
						signalStrengthEntries.get(2).dateTime.getHourOfDay(),
						signalStrengthEntries.get(2).dateTime.getMinuteOfHour(),
						signalStrengthEntries.get(2).dateTime.getSecondOfMinute(),
						signalStrengthEntries.get(1).signalStrengthLevel)));
		assertThat("Third data set starts on the third and ends on the fourth log item",
				clockViewHelpers.get(2),
				samePropertyValuesAs(new ClockViewHelper(
						signalStrengthEntries.get(2).dateTime.getHourOfDay(),
						signalStrengthEntries.get(2).dateTime.getMinuteOfHour(),
						signalStrengthEntries.get(2).dateTime.getSecondOfMinute(),
						signalStrengthEntries.get(4).dateTime.getHourOfDay(),
						signalStrengthEntries.get(4).dateTime.getMinuteOfHour(),
						signalStrengthEntries.get(4).dateTime.getSecondOfMinute(),
						signalStrengthEntries.get(2).signalStrengthLevel)));
	}

	@Test
	public void getHelperDataWithSpecificDataThatIncludesApplicationServicesChangedEventsAndLogsBetween() {
		List<SignalStrengthEntry> signalStrengthEntries = new ArrayList<>();
		List<ApplicationServicesChangedEvent> applicationServicesChangedEvents = new ArrayList<>();
		signalStrengthEntries.add(new SignalStrengthEntry(4, DateTime.parse("2019-07-22T00:00:00.000-07:00")));

		applicationServicesChangedEvents.add(new ApplicationServicesChangedEvent(DateTime.parse("2019-07-22T00:30:00.000-07:00"), ApplicationServicesChangedEvent.ApplicationServicesChangedEventType.PREFERENCE_OFF));
		// Each of these three signal strength entries should be ignored since the preference is turned off
		// This scenario shouldn't happen though since the services would be off until they're restored
		signalStrengthEntries.add(new SignalStrengthEntry(4, DateTime.parse("2019-07-22T00:00:35.000-07:00")));
		signalStrengthEntries.add(new SignalStrengthEntry(4, DateTime.parse("2019-07-22T00:00:40.000-07:00")));
		signalStrengthEntries.add(new SignalStrengthEntry(4, DateTime.parse("2019-07-22T00:00:45.000-07:00")));
		applicationServicesChangedEvents.add(new ApplicationServicesChangedEvent(DateTime.parse("2019-07-22T00:30:00.000-07:00"), ApplicationServicesChangedEvent.ApplicationServicesChangedEventType.PREFERENCE_ON));

		signalStrengthEntries.add(new SignalStrengthEntry(3, DateTime.parse("2019-07-22T01:00:00.000-07:00")));
		signalStrengthEntries.add(new SignalStrengthEntry(4, DateTime.parse("2019-07-22T02:00:00.000-07:00")));
		signalStrengthEntries.add(new SignalStrengthEntry(4, DateTime.parse("2019-07-22T03:00:00.000-07:00")));
		signalStrengthEntries.add(new SignalStrengthEntry(2, DateTime.parse("2019-07-22T04:00:00.000-07:00")));

		List<ClockViewHelper> clockViewHelpers = ClockViewHelper.getHelperData(signalStrengthEntries, applicationServicesChangedEvents);
		assertEquals("There should be three sections. The final entry doesn't have an \"end\" time",
				3,
				clockViewHelpers.size());
		assertThat("First data set ends when the monitoring preference is changed to off",
				clockViewHelpers.get(0),
				samePropertyValuesAs(new ClockViewHelper(
						signalStrengthEntries.get(0).dateTime.getHourOfDay(),
						signalStrengthEntries.get(0).dateTime.getMinuteOfHour(),
						signalStrengthEntries.get(0).dateTime.getSecondOfMinute(),
						applicationServicesChangedEvents.get(0).datetime.getHourOfDay(),
						applicationServicesChangedEvents.get(0).datetime.getMinuteOfHour(),
						applicationServicesChangedEvents.get(0).datetime.getSecondOfMinute(),
						signalStrengthEntries.get(0).signalStrengthLevel)));
		assertThat("Second data set begins at the first log item after the monitoring preference is changed to on and ends on the next log item after that",
				clockViewHelpers.get(1),
				samePropertyValuesAs(new ClockViewHelper(
						signalStrengthEntries.get(4).dateTime.getHourOfDay(),
						signalStrengthEntries.get(4).dateTime.getMinuteOfHour(),
						signalStrengthEntries.get(4).dateTime.getSecondOfMinute(),
						signalStrengthEntries.get(5).dateTime.getHourOfDay(),
						signalStrengthEntries.get(5).dateTime.getMinuteOfHour(),
						signalStrengthEntries.get(5).dateTime.getSecondOfMinute(),
						signalStrengthEntries.get(4).signalStrengthLevel)));
		assertThat("Third data set starts on the third and ends on the fourth log item",
				clockViewHelpers.get(2),
				samePropertyValuesAs(new ClockViewHelper(
						signalStrengthEntries.get(5).dateTime.getHourOfDay(),
						signalStrengthEntries.get(5).dateTime.getMinuteOfHour(),
						signalStrengthEntries.get(5).dateTime.getSecondOfMinute(),
						signalStrengthEntries.get(7).dateTime.getHourOfDay(),
						signalStrengthEntries.get(7).dateTime.getMinuteOfHour(),
						signalStrengthEntries.get(7).dateTime.getSecondOfMinute(),
						signalStrengthEntries.get(5).signalStrengthLevel)));
	}

}