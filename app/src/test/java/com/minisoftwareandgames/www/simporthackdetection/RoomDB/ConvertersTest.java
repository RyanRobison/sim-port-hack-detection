package com.minisoftwareandgames.www.simporthackdetection.RoomDB;

import org.joda.time.DateTime;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ConvertersTest {

	private static final Long dateTimeLong = 1558741682952L;

	@Test
	public void testTypeConverterDateTimeToFromLong() {
		DateTime dateTime = new DateTime(dateTimeLong);
		assertEquals(dateTimeLong, Converters.dateToTimestamp(dateTime));
		assertEquals(dateTime, Converters.fromTimestamp(dateTimeLong));
	}

	@Test
	public void testDateToUiString() {
		DateTime dateTime = new DateTime(dateTimeLong);
		assertEquals("16:48:02 Fri, May 24, 2019", Converters.dateToUiString(dateTime));
	}

	@Test
	public void testDateToNotificationString() {
		DateTime dateTime = new DateTime(dateTimeLong);
		assertEquals("16:48:02", Converters.dateToNotificationString(dateTime));
	}

	@Test
	public void testApplicationServicesChangedEventTypeToStringTypeConverter() {
		assertEquals(ApplicationServicesChangedEvent.ApplicationServicesChangedEventType.DEVICE_OFF_TAG, Converters.applicationServicesChangedEventTypeToString(ApplicationServicesChangedEvent.ApplicationServicesChangedEventType.DEVICE_OFF));
		assertEquals(ApplicationServicesChangedEvent.ApplicationServicesChangedEventType.DEVICE_ON_TAG, Converters.applicationServicesChangedEventTypeToString(ApplicationServicesChangedEvent.ApplicationServicesChangedEventType.DEVICE_ON));
		assertEquals(ApplicationServicesChangedEvent.ApplicationServicesChangedEventType.PREFERENCE_OFF_TAG, Converters.applicationServicesChangedEventTypeToString(ApplicationServicesChangedEvent.ApplicationServicesChangedEventType.PREFERENCE_OFF));
		assertEquals(ApplicationServicesChangedEvent.ApplicationServicesChangedEventType.PREFERENCE_ON_TAG, Converters.applicationServicesChangedEventTypeToString(ApplicationServicesChangedEvent.ApplicationServicesChangedEventType.PREFERENCE_ON));
	}

	@Test
	public void testApplicationServicesChangedEventTypeFromStringTypeConverter() {
		assertEquals(ApplicationServicesChangedEvent.ApplicationServicesChangedEventType.DEVICE_OFF, Converters.applicationServicesChangedEventTypeFromString(ApplicationServicesChangedEvent.ApplicationServicesChangedEventType.DEVICE_OFF_TAG));
		assertEquals(ApplicationServicesChangedEvent.ApplicationServicesChangedEventType.DEVICE_ON, Converters.applicationServicesChangedEventTypeFromString(ApplicationServicesChangedEvent.ApplicationServicesChangedEventType.DEVICE_ON_TAG));
		assertEquals(ApplicationServicesChangedEvent.ApplicationServicesChangedEventType.PREFERENCE_OFF, Converters.applicationServicesChangedEventTypeFromString(ApplicationServicesChangedEvent.ApplicationServicesChangedEventType.PREFERENCE_OFF_TAG));
		assertEquals(ApplicationServicesChangedEvent.ApplicationServicesChangedEventType.PREFERENCE_ON, Converters.applicationServicesChangedEventTypeFromString(ApplicationServicesChangedEvent.ApplicationServicesChangedEventType.PREFERENCE_ON_TAG));
	}

}