# SIM Port Hack Detection
**SIM Port Hack Detection** is an Android application built to provide client-side monitoring for connectivity to your mobile service provider. The first signs of a SIM Port Hack, as well illustrated in [Sean Coonce](https://medium.com/@cooncesean) in his [Medium article](https://medium.com/coinmonks/the-most-expensive-lesson-of-my-life-details-of-sim-port-hack-35de11517124), is losing connection to your mobile service provider. When a close family friend shared they were under attack, I began brainstorming ways to protect my friends and family. I imagine the feeling of receiving email notifications of "*your password has been changed*" with no recourse is similar to being burglarized, except that the internet is associated with every aspect of our lives; you can't simply move to a safer neighborhood.

The notion of a [heartbeat monitor](https://en.wikipedia.org/wiki/Heartbeat_(computing)) is not unfamiliar to anybody in the software industry who's had to support a production system. My solution was to install an app on my phone that would send text messages to myself every 5 minutes: a solution [Chris Robison](https://medium.com/@cbobrobison) included in his [Medium article](https://medium.com/@CBobRobison/youre-under-a-sim-port-attack-here-s-how-to-fight-back-now-d89b87ce4960) about next steps when undergoing an attack. However, sending yourself texts every 5 minutes isn't a solution for anybody with limited monthly texting and begins onsetting some serious paranoia. That's how I came up with the idea for **SIM Port Hack Detection**.

# Features
#### Monitor Signal Strength, 24/7
A foreground process monitors the strength of your signal anytime it changes and updates in your notification drawer with the most recent status.

#### Persistent Loss of Service Notification
In the case that your device loses service, you will be notified with a continuous notification. This is designed to continue until you've engaged with the notification, otherwise using your default notification settings.

#### Dark Theme
Because some people just really like dark themes.

# Releases
The first release to the Google Play Store is expected to be in July of 2019. Release versioning follows the pattern of `Major.Minor.Revision`

| Release Version | Release Date | Corresponding Milestone |
| :--- | :---: | ---: |
| 1.0.0 | July 01, 2019 | [Minimum Viable Product](https://gitlab.com/RyanRobison/sim-port-hack-detection/-/milestones/1) |
| 1.0.1 | July 07, 2019 | [Alpha Release 2](https://gitlab.com/RyanRobison/sim-port-hack-detection/-/milestones/3) |
| 1.1.0 | July 29, 2019 | [Alpha Release 3](https://gitlab.com/RyanRobison/sim-port-hack-detection/-/milestones/4) |

If interested in participating in the closed alpha testing, please reach out to [minisoftwareandgames@gmail.com](minisoftwareandgames@gmail.com) to be added to the list.

# Author
Ryan Robison